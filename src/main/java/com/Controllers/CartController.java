/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import com.DAOS.FoodDAO;
import com.DAOS.OrderDAO;
import com.DAOS.TableDAO;
import com.models.Food;
import com.models.Order_items;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author cuongseven
 */
public class CartController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();

        if (path.startsWith("/cart")) {

            /* ------ Check access từ Payment sang -------*/
            // Truy cập không hợp lý,    
            HttpSession session = request.getSession();
            if (session.getAttribute("InvoiceID_number") == null) {
                session.setAttribute("error", "..We detect you access illegal");
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            } else {
                //Còn nếu đúng các bước thì tới trang này
                request.getRequestDispatcher("cart.jsp").forward(request, response);

            }

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        HttpSession session = request.getSession();
        TableDAO tableDAO = new TableDAO();
        OrderDAO orderDAO = new OrderDAO();
        FoodDAO foodDAO = new FoodDAO();

        //Lấy data từ session
        String orderID = session.getAttribute("orderID").toString();
        String tableDate = session.getAttribute("table_date").toString();
        String timeCheckin = session.getAttribute("timeCheckin").toString();
        String people = session.getAttribute("people_number").toString();
        String note = session.getAttribute("note").toString();
        String InvoiceID_number = session.getAttribute("InvoiceID_number").toString();

        String btnGoToPayment = request.getParameter("btnGoToPayment");

        if (path.startsWith("/cart")) {
            if (btnGoToPayment != null) {

                //Lấy FoodID, Food Amount từ Menu truyền vào Cart. 
                try {
                    String foodIdAmount = request.getParameter("FoodID_Amount").substring(1);
                    if (foodIdAmount != null) {
                        String[] arr = foodIdAmount.split("/"); //Chuổi DATA có format :   "/f1/1/f2/1/f2/2/f2/3"
                        HashMap<String, Integer> cart = new HashMap<>();

                        for (int i = 0; i < arr.length; i++) {
                            if (i % 2 == 0) {// Loại bỏ first element
                                cart.put(arr[i], 0);
                            } else {// Food amount
                                int foodAmount = Integer.parseInt(arr[i]);

                                //Tránh bị out length
                                if (i - 1 >= 0 && i - 1 < arr.length) {
                                    String foodId = arr[i - 1];
                                    cart.put(foodId, foodAmount);

                                    //CheckExist to update f_amount
                                    if (foodDAO.isExistsFoodID(foodId, orderID)) {
                                        foodDAO.updateFoodAmount(foodAmount, foodId, orderID);
                                    }
                                    foodDAO.addFoodInfor(new Order_items(foodId, foodAmount, orderID));
                                }
                            }
                        }

                        /*----------------PHẦN SHOW THÔNG TIN INVOICE --- TÍNH TỔNG TIỀN THANH TOÁN----------------*/
                        float totalPayment = 0;

                        for (Map.Entry<String, Integer> currentMap : cart.entrySet()) {
                            String foodID = currentMap.getKey();
                            int foodAmount = currentMap.getValue();

                            Food f = foodDAO.getFoodByID(foodID);
                            float totalFoodPrice = foodAmount * f.getFood_price();
                            totalPayment += totalFoodPrice;
                        }

                        // Update the total payment and save the cart and total payment to the session
                        orderDAO.updateInvoiceTotal(totalPayment, InvoiceID_number);
                        session.setAttribute("cartMenu", cart);
                        session.setAttribute("totalPayment", totalPayment);
                    }
                } catch (StringIndexOutOfBoundsException e) {  // >>>> use try-catch to catch error when no enter food_amount <<< Javascript handle at Menupage. not needed.
                    session.setAttribute("error", "Please you enter food quantity.");
                    request.getRequestDispatcher("/error.jsp").forward(request, response);
                }
            }

            //Đúng các bước sẽ chuyển đến trang Payment
            response.sendRedirect("/payment");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
