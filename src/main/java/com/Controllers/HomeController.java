package com.Controllers;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author giacu
 */
public class HomeController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String type = request.getParameter("type");
        HttpSession session = request.getSession();
        // ========== nav home ========== //
        if (type == null) {
            //Nếu tới Payment mà Click vào lại Menu để chọn món,
            if (session.getAttribute("InvoiceID_number") != null) {
                session.setAttribute("backToPayment", "active");
                session.setAttribute("error", "Please done order before start an new order");
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            } else {
                //Còn nếu đúng các bước thì tới Menu chọn món bình thường.!
                request.getRequestDispatcher("/home.jsp").forward(request, response);

            }
        } else {
            // ========== home ========== //
//            if (type.equalsIgnoreCase("/home")) {
//                request.getRequestDispatcher("/home.jsp").forward(request, response);
//            }
            // ========== nav menu ========== //
            if (type.equalsIgnoreCase("menu")) {
                request.getRequestDispatcher("/menu.jsp").forward(request, response);
            }
            // ========== nav cart ========== //
            if (type.equalsIgnoreCase("cart")) {
                request.getRequestDispatcher("/cart.jsp").forward(request, response);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // ========== update account info ========== //
        if (request.getParameter("btn_payment") != null) {
            request.getRequestDispatcher("/home.jsp").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
