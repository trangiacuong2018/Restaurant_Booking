package com.Controllers;

import com.DAOS.FoodDAO;
import com.DAOS.OrderDAO;
import com.DAOS.TableDAO;
import com.models.Food;
import com.models.Invoice;
import com.models.Order_items;
import com.models._Order;
import com.models._Table;
import java.util.Date;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author cuongseven
 */
public class MenuController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request request
     * @param response response
     * @throws ServletException if a -specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Declare varible
        List<Food> list;
        String path = request.getRequestURI();
        HttpSession session = request.getSession();
//------------------------------------------------------------------------
//GET useID from cookie. setted at menu-management page.
        Cookie[] cookiess = request.getCookies();
        String userID = "";
        if (cookiess != null) {
            for (Cookie cookie : cookiess) {
                if (cookie.getName().equals("account")) {
                    userID = cookie.getValue();
                    break;
                }
            }
        }
//------------------------------------------------------------------------        
        if (path.equalsIgnoreCase("/menu")) {
            FoodDAO foodDAO = new FoodDAO();
            list = foodDAO.getAllFood();
            request.setAttribute("listFood", list);

            //Nếu tới Payment mà Click vào lại Menu để chọn món,
            if (session.getAttribute("InvoiceID_number") != null) {
                session.setAttribute("backToPayment", "active");
                session.setAttribute("error", "Please done order before start an new order");
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            } else {
                //Còn nếu đúng các bước thì tới Menu chọn món bình thường.!
                request.getRequestDispatcher("/menu.jsp").forward(request, response);
            }

        } else if (path.startsWith("/menu/cart")) {
//            String cart = request.getAttribute("hsmfromPayment").toString();
            request.getRequestDispatcher("/cart.jsp").forward(request, response);

        } else if (path.startsWith("/menu/payment")) {
            request.getRequestDispatcher("/payment.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request request
     * @param response response
     * @throws ServletException if a -specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //  GET useID from cookie. setted at menu-management page.
        Cookie[] cookiess = request.getCookies();
        String userID = "";
        if (cookiess != null) {
            for (Cookie cookie : cookiess) {
                if (cookie.getName().equals("account")) {
                    userID = cookie.getValue();
                    break;
                }
            }
        }

        String path = request.getRequestURI();
        HttpSession session = request.getSession();

        String btnGoToPayment = request.getParameter("btnGoToPayment");

        if (!userID.equals("")) {

            if (btnGoToPayment != null) {
                if (path.startsWith("/menu")) {

                    if (session.getAttribute("tableID") != null && session.getAttribute("fullName") != null) { // chặn user đã login nhưng click thẳng vào Menu button --> go to Payment.

                        /* Lấy DateTime hiện tại.*/
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = new Date();
                        String currentDate = dateFormat.format(date);
                        TableDAO tableDAO = new TableDAO();
                        OrderDAO orderDAO = new OrderDAO();
                        FoodDAO foodDAO = new FoodDAO();

                        //----- Cách 1: Random InvoiceID--------------------- Running ---
                        int min = 1;
                        int max = 999;
                        int invoiceIDRandom = (int) Math.floor(Math.random() * (max - min + 1) + min);
                        String InvoiceID_number = "i" + (invoiceIDRandom + 1);

                        /*-------Cách 2: tạo orderID tăng theo dần-------------- OK ----*/
//                        int i_number = orderDAO.getNumberOfInvoice();
//                        String InvoiceID_number = "i" + (i_number + 1);
//
//                        boolean oid_Exists = orderDAO.isExistsInvoiceID(InvoiceID_number);
//                        if (oid_Exists) {
//                            //nếu tồn tại thì (numberoforderID) +2  | phải +2
//                            InvoiceID_number = "i" + (i_number + 2);
//                        }
                        /*--------------------------------------------------------*/
                        //Lấy số bàn dựa vào TableID
                        String tableID = session.getAttribute("tableID").toString();
                        _Table table = tableDAO.getTableByID(tableID);
                        int table_number = table.getTable_number();

                        //Lấy data từ session
                        String orderID = session.getAttribute("orderID").toString();
                        String tableDate = session.getAttribute("table_date").toString();
                        String timeCheckin = session.getAttribute("timeCheckin").toString();
                        String people = session.getAttribute("people_number").toString();
                        String note = session.getAttribute("note").toString();

                        //Tạo invoice
                        float initTotalPayment = 0;
                        orderDAO.addNewInvoice(new Invoice(InvoiceID_number, initTotalPayment, currentDate));
                        tableDAO.updateTable(tableID, "busy");

                        //Tạo Order
                        orderDAO.addNewOrder(new _Order(orderID, "Processing", table_number, Integer.parseInt(people), tableDate, timeCheckin, note, InvoiceID_number, userID, tableID));
                        //Lấy FoodID, Food Amount từ Menu truyền vào Cart. 
                        try {
                            String foodIdAmount = request.getParameter("FoodID_Amount").substring(1);
                            if (foodIdAmount != null) {
                                String[] arr = foodIdAmount.split("/"); //Chuổi DATA có format :   "/f1/1/f2/1/f2/2/f2/3"
                                HashMap<String, Integer> cart = new HashMap<>();

                                for (int i = 0; i < arr.length; i++) {
                                    if (i % 2 == 0) {// Loại bỏ first element
                                        cart.put(arr[i], 0);
                                    } else {// Food amount
                                        int foodAmount = Integer.parseInt(arr[i]);

                                        //Tránh bị out length
                                        if (i - 1 >= 0 && i - 1 < arr.length) {
                                            String foodId = arr[i - 1];
                                            cart.put(foodId, foodAmount);

                                            //CheckExist to update f_amount
                                            if (foodDAO.isExistsFoodID(foodId, orderID)) {
                                                foodDAO.updateFoodAmount(foodAmount, foodId, orderID);
                                            }
                                            foodDAO.addFoodInfor(new Order_items(foodId, foodAmount, orderID));
                                        }
                                    }
                                }

                                /*----------------PHẦN SHOW THÔNG TIN INVOICE --- TÍNH TỔNG TIỀN THANH TOÁN----------------*/
                                float totalPayment = 0;

                                for (Map.Entry<String, Integer> currentMap : cart.entrySet()) {
                                    String foodID = currentMap.getKey();
                                    int foodAmount = currentMap.getValue();

                                    Food f = foodDAO.getFoodByID(foodID);
                                    float totalFoodPrice = foodAmount * f.getFood_price();
                                    totalPayment += totalFoodPrice;
                                }

                                // Update the total payment and save the cart and total payment to the session
                                orderDAO.updateInvoiceTotal(totalPayment, InvoiceID_number);
                                session.setAttribute("cartMenu", cart);
                                session.setAttribute("totalPayment", totalPayment);
                                session.setAttribute("InvoiceID_number", InvoiceID_number);

                            }
                        } catch (StringIndexOutOfBoundsException e) {  // >>>> use try-catch to catch error when no enter food_amount <<< Javascript handle at Menupage. not needed.
                            session.setAttribute("error", "Please you enter food quantity.");
                            request.getRequestDispatcher("/error.jsp").forward(request, response);
                        }

                        //Đúng các bước sẽ chuyển đến trang Payment
//                        request.getRequestDispatcher("/payment").forward(request, response);
                        response.sendRedirect("/payment");
                    } else {
                        session.setAttribute("error", "Please you back to book a table and enter full information before go to payment");
                        request.getRequestDispatcher("/error.jsp").forward(request, response);
                    }
                }
            }

        } else {
            // ERROR Click [Go to payment],but login not yet !!
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the .
     *
     * @return a String containing description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
