/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import com.DAOS.MenuManageDAO;
import com.models.Food;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Hung
 */
public class MenuManageController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String type = request.getParameter("type");
        if (type == null) {
            request.setAttribute("msg", "");
            request.getRequestDispatcher("/menu-management.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String f_id, f_name, f_img, f_desc, f_stat, f_price, c_id, msg = "";
        MenuManageDAO m = new MenuManageDAO();
        if (request.getParameter("btnFAdd") != null) {
            f_id = "f" + request.getParameter("nextFID");
            f_name = request.getParameter("txtFName");
            f_img = request.getParameter("txtFImg");
            f_desc = request.getParameter("txtFDesc");
            f_stat = request.getParameter("txtFStat");
            f_price = request.getParameter("txtFPrice");
            c_id = request.getParameter("txtFCat");

            if (m.checkFoodName(f_name)) {
                Food f = new Food(f_id, f_name, f_img, f_desc, f_stat, Float.parseFloat(f_price), c_id);
                MenuManageDAO dao = new MenuManageDAO();
                dao.addFood(f);
                msg = "";
                request.setAttribute("msg", msg);
                response.sendRedirect("/menu-management");
            } else {
                msg = "*This food has existed!";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("/menu-management.jsp").forward(request, response);
            }
        }

        if (request.getParameter("btnFUpdate") != null) {
            f_id = request.getParameter("txtFID");
            f_name = request.getParameter("txtFName");
            f_img = request.getParameter("txtFImg");
            f_desc = request.getParameter("txtFDesc");
            f_stat = request.getParameter("txtFStat");
            f_price = request.getParameter("txtFPrice");
            c_id = request.getParameter("txtFCat");

            if (m.checkFoodName(f_name)) {
                Food f = new Food(f_id, f_name, f_img, f_desc, f_stat, Float.parseFloat(f_price), c_id);
                MenuManageDAO dao = new MenuManageDAO();
                dao.updateFood(f);
                msg = "";
                request.setAttribute("msg", msg);
                response.sendRedirect("/menu-management");
            } else {
                msg = "*This food has existed!";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("/menu-management.jsp").forward(request, response);
            }
        }

        if (request.getParameter("btnFDel") != null) {
            f_id = request.getParameter("txtFID");

            MenuManageDAO dao = new MenuManageDAO();
            dao.deleteFood(f_id);
            response.sendRedirect("/menu-management");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
