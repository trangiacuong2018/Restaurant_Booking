/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author cuongseven
 */
public class OrderController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String path = request.getRequestURI();

        HttpSession session = request.getSession();
        session.getAttribute("tableID");
        if (session.getAttribute("tableID") != null) {

            if (path.startsWith("/order")) {

                /* ------ Check access từ Payment sang -------*/
                //Nếu tới Payment mà Click vào lại Menu để chọn món,    
                if (session.getAttribute("InvoiceID_number") != null) {
                    session.setAttribute("backToPayment", "active");
                    session.setAttribute("error", "Please complete your order before creating a new order");
                    request.getRequestDispatcher("/error.jsp").forward(request, response);
                } else {
                    //Đúng các bước thì đến trang này
                    request.getRequestDispatcher("/form-booking.jsp").forward(request, response);
                }

            } //else, you at form-booking, but not booked that "change path" will be not booked successful 
            else {
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            }
        } else {
            session.setAttribute("error", "..We detect you access illegal");
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String path = request.getRequestURI();

        if (path.startsWith("/order/")) {
            if (request.getParameter("btnSubmit") != null) {
//                String userID = request.getParameter("txtUserID");
                String phone = request.getParameter("txtPhone");
                String fullName = request.getParameter("txtFullName");

                String table_date = request.getParameter("txtDate");
                String timeCheckin = request.getParameter("txtTime");
                int people_number = Integer.parseInt(request.getParameter("txtPeople"));
                String note = request.getParameter("txtNote");

                //Set Session truyền data.
                HttpSession sess = request.getSession();

                sess.setAttribute("phone", phone);
                sess.setAttribute("fullName", fullName);
                sess.setAttribute("table_date", table_date);
                sess.setAttribute("timeCheckin", timeCheckin);
                sess.setAttribute("people_number", people_number);
                sess.setAttribute("note", note);
                
                response.sendRedirect("/menu");
            }
        }

        // ========== update account info ========== //
        if (request.getParameter("btn_view_food") != null) {
            // init parameter
            String temp = request.getParameter("btn_view_food");
            request.setAttribute("orderId", temp);
            request.getRequestDispatcher("/view-food-booked.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
