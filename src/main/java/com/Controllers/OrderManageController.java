/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import com.DAOS.OrderManageDAO;
import com.DAOS.TableDAO;
import com.models._Table;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Hung
 */
public class OrderManageController extends HttpServlet {

    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        List<_Table> list;

        if (path.startsWith("/orders-management")) {
            TableDAO tableDAO = new TableDAO();
            list = tableDAO.getAllTables();
            request.setAttribute("list", list);
            request.getRequestDispatcher("/orders-management.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String o_id, t_id;
        List<_Table> list;

        if (request.getParameter("btnODel") != null) {
            o_id = request.getParameter("txtOID");
            t_id = request.getParameter("txtTID");

            OrderManageDAO dao = new OrderManageDAO();
            dao.deleteOrder(o_id);
            dao.changeTableStatus(t_id);
            response.sendRedirect("/orders-management");
        }

        if (request.getParameter("btnTableID") != null) {
            t_id = request.getParameter("btnTableID");

            OrderManageDAO dao = new OrderManageDAO();
            dao.changeTableStatus(t_id);
            response.sendRedirect("/orders-management");
        }

        if (request.getParameter("btnOCon") != null) {
            o_id = request.getParameter("txtOID");

            OrderManageDAO dao = new OrderManageDAO();
            dao.changeOrderStatus(o_id);
            response.sendRedirect("/orders-management");
        }

        if (request.getParameter("btnODetail") != null) {
            o_id = request.getParameter("btnODetail");
            request.setAttribute("oid", o_id);

            TableDAO tableDAO = new TableDAO();
            list = tableDAO.getAllTables();
            request.setAttribute("list", list);
            request.getRequestDispatcher("/orders-management.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
