/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import com.DAOS.FoodDAO;
import com.DAOS.OrderDAO;
import com.DAOS.TableDAO;
import com.models._Table;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author cuongseven
 */
public class PaymentController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String path = request.getRequestURI();
        HttpSession session = request.getSession();

        if (path.startsWith("/payment")) {

            /* ------ Check access từ Payment sang -------*/
            // Truy cập không hợp lý,    
            if (session.getAttribute("InvoiceID_number") == null) {
                session.setAttribute("error", "..We detect you access illegal");
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            } else {
                //Còn nếu đúng các bước thì tới trang này
                request.getRequestDispatcher("/payment.jsp").forward(request, response);

            }

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        HttpSession session = request.getSession();
        String btnPayment = request.getParameter("btnPayment");
        String btnCancel = request.getParameter("btnCancel");

        //Gọi lớp DAO
        TableDAO tableDAO = new TableDAO();
        OrderDAO orderDAO = new OrderDAO();
        FoodDAO foodDAO = new FoodDAO();

        //Lấy số bàn dựa vào TableID
        String tableID = session.getAttribute("tableID").toString();
        _Table table = tableDAO.getTableByID(tableID);
        int table_number = table.getTable_number();

        //Lấy data từ session
        String orderID = session.getAttribute("orderID").toString();
        String tableDate = session.getAttribute("table_date").toString();
        String timeCheckin = session.getAttribute("timeCheckin").toString();
        String people = session.getAttribute("people_number").toString();
        String note = session.getAttribute("note").toString();
        String InvoiceID_number = session.getAttribute("InvoiceID_number").toString();

        if (path.startsWith("/payment")) {

            if (btnPayment != null) {

            }
            if (btnCancel != null) {
                orderDAO.deleteOrderItemsByOID(orderID);
                orderDAO.deleteOrderByID(orderID);
                orderDAO.deleteInvoiceByID(InvoiceID_number);
                tableDAO.updateTable(tableID, "available");
            }

//            session.invalidate();
            session.setAttribute("orderID", null);
            session.setAttribute("tableID", null);
            session.setAttribute("table_date", null);
            session.setAttribute("timeCheckin", null);
            session.setAttribute("people_number", null);
            session.setAttribute("note", null);
            session.setAttribute("totalPayment", null);
            session.setAttribute("cart", null);
            session.setAttribute("InvoiceID_number", null);
            session.setAttribute("fullName", null);
            session.setAttribute("phone", null);
            session.setAttribute("backToPayment", null);
            session.setAttribute("error", null);
        }
        response.sendRedirect("/");
        //KẾT THÚC : Go back HomePage 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
