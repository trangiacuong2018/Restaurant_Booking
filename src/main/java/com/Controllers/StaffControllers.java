/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.Controllers;

import com.DAOS.AccountDAO;
import com.DAOS.UserDAO;
import com.models._User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class StaffControllers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StaffControllers</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StaffControllers at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("msg", "");
        request.getRequestDispatcher("/staff-account-management.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, UnsupportedEncodingException {
        PrintWriter out = response.getWriter();
        if (request.getParameter("btn_add_staff") != null) {
            String user_id, fullname, email, user_role, phone, username, password, msg="";
            user_id = generateID();
            fullname = request.getParameter("full_name");
            email = request.getParameter("email");
            user_role = "staff";
            phone = request.getParameter("phone");
            username = request.getParameter("username");
            password = request.getParameter("password");
            UserDAO userDAO = new UserDAO();

            AccountDAO accountDAO = new AccountDAO();
            if (accountDAO.checkAccountExist(username)) {
                msg = "*This username has existed!";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("/staff-account-management.jsp").forward(request, response);
            } else {

                _User user = new _User(user_id, fullname, email, user_role, phone, username, password);
                try {
                    int check = userDAO.addUser(user);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(StaffControllers.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(request.getContextPath() + "/staff-management");
            }
        }

        if (request.getParameter("btn_delete_staff") != null) {
            String user_id = request.getParameter("user_id");
            UserDAO userDAO = new UserDAO();
            int check = userDAO.deleteUser(user_id);
            response.sendRedirect(request.getContextPath() + "/staff-management");
        }

        if (request.getParameter("btn_update_staff") != null) {
            String user_id, fullname, email, user_role, phone, username, password;
            user_id = request.getParameter("user_id");
            fullname = request.getParameter("full_name");
            email = request.getParameter("email");
            user_role = "staff";
            phone = request.getParameter("phone");
            username = request.getParameter("username");
            password = request.getParameter("password");
            UserDAO userDAO = new UserDAO();
            _User user = new _User(user_id, fullname, email, user_role, phone, username, password);
            try {
                int check = userDAO.updateUser(user);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(StaffControllers.class.getName()).log(Level.SEVERE, null, ex);
            }
            response.sendRedirect(request.getContextPath() + "/staff-management");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String generateID() {
        String id = "";
        int min = 1;
        int max = 100;
        UserDAO aO = new UserDAO();
        while (true) {
            int random_int = (int) (Math.random() * (max - min + 1) + min);
            id += "u" + String.valueOf(random_int);
            if (aO.checkIDUser(id)) {
                break;
            }
        }
        return id;
    }

}
