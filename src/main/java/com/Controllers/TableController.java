package com.Controllers;

import com.DAOS.OrderDAO;
import com.DAOS.TableDAO;
import com.models._Table;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author cuongseven
 */
public class TableController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        List<_Table> list;
        HttpSession session = request.getSession();
        //GET useID from cookie. setted at menu-management page.
        Cookie[] cookiess = request.getCookies();
        String userID = "";
        if (cookiess != null) {
            for (Cookie cookie : cookiess) {
                if (cookie.getName().equals("account")) {
                    userID = cookie.getValue();
                    break;
                }
            }
        }

        //Check User isLogin? to bookking a table.
        if (!userID.equals("")) {
            String[] s = path.split("/");
            String tableID = s[s.length - 1];

            //Just can be access with path are /table or /table/+ TableID, if not, ERROR
            if (path.startsWith("/table") || path.startsWith("/table/" + tableID)) {
                if (path.endsWith("/table")) {
                    TableDAO tableDAO = new TableDAO();
                    list = tableDAO.getAllTables();
                    request.setAttribute("list", list);

                    /* ------ Check access từ Payment sang -------*/
                    //Nếu tới Payment mà Click vào lại Menu để chọn món,    
                    if (session.getAttribute("InvoiceID_number") != null) {
                        session.setAttribute("backToPayment", "active");
                        session.setAttribute("error", "Please done order before start an new order");
                        request.getRequestDispatcher("/error.jsp").forward(request, response);
                    } else {
                        //Còn nếu đúng các bước thì tới trang này
                        request.getRequestDispatcher("/booking-a-table.jsp").forward(request, response);

                    }

                }

                if (path.startsWith("/table/")) {

                    TableDAO tableDAO = new TableDAO();
                    OrderDAO orderDAO = new OrderDAO();

                    _Table table = tableDAO.getTableByID(tableID);
                    if (table != null) {

                        /*---cách 1: tạo orderID ngẩu nhiên (KHÔNG TRÙNG LẶP)---*/
//                        boolean isLoop = true;
//                        while (isLoop) {
//                            int min = 1;
//                            int max = 999;
//                            int o_idRandom = (int) Math.floor(Math.random() * (max - min + 1) + min);
//                            String OrderID_number = "o" + (o_idRandom + 1);
//
//                            boolean oid_Exists = orderDAO.isExistsOrderID(OrderID_number);
//                            if (!oid_Exists) {
//
//                                //lưu session orderID, tableID vào brower
//                                HttpSession sess = request.getSession();
//                                sess.setAttribute("orderID", OrderID_number);
//                                sess.setAttribute("tableID", tableID);
//
//                                // Chuyển hướng đến trang "/order/orderID"
//                                response.sendRedirect("/order/" + OrderID_number);
//
//                                //Phải có return để trả về
//                                return;
//                            }
//
//                        }
                        /*-------Cách2: tạo orderID tăng theo dần----*/
                        boolean isLoop = true;
                        while (isLoop) {
                            int o_number = orderDAO.getNumberOfOrder();
                            String OrderID_number = "o" + (o_number + 1);

                            boolean oid_Exists = orderDAO.isExistsOrderID(OrderID_number);
                            if (oid_Exists) {
                                //nếu tồn tại thì (numberoforderID) +2  | phải +2
                                OrderID_number = "o" + (o_number + 2);
                            }
                            //lưu session orderID, tableID vào brower
                            HttpSession sess = request.getSession();
                            sess.setAttribute("orderID", OrderID_number);
                            sess.setAttribute("tableID", tableID);

                            // Chuyển hướng đến trang "/order/orderID"
                            response.sendRedirect("/order/" + OrderID_number);

                            //Phải có return để trả về /return là lệnh để thoát vòng lập lập tức. nếu ko sẽ bị lỗi sendRedirect [IllegalStateException] 
                            return;
                        }

                    }
                }
            } else {
                request.getRequestDispatcher("/error.jsp").forward(request, response);

            }
        } else {
            response.sendRedirect("/account/login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
