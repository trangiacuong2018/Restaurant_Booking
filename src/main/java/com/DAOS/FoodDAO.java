package com.DAOS;

import com.DBConnection.DBConnection;
import com.models.Food;
import com.models.Order_items;
import com.models._Order;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cuongseven
 */
public class FoodDAO {

    private Connection connection = null;

    public FoodDAO() {
        connection = DBConnection.getConnection();
    }

    public List<Food> getAllFood() {
        List<Food> listFood = new ArrayList<>();
        try {
            Statement st = null;
            ResultSet rs = null;
            String query = "SELECT * FROM Food";
            st = connection.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                listFood.add(new Food(rs.getString("food_id"), rs.getString("food_name"), rs.getString("food_image"), rs.getString("food_desc"), rs.getString("food_status"), rs.getFloat("food_price"), rs.getString("category_id")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listFood;
    }

    public int addFoodInfor(Order_items o_item) {

        int count = 0;
        String query = "INSERT INTO `Order_items` (`food_id`,`food_amount`,`order_id`) VALUES (?,?,?)";
        try ( PreparedStatement pst = connection.prepareStatement(query)) {
            pst.setString(1, o_item.getFood_id());
            pst.setInt(2, o_item.getFood_amount());
            pst.setString(3, o_item.getOrder_id());

            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public HashMap<String, Order_items> addFoodToCart(String foodID, int food_amount, String orderID) {

        HashMap<String, Order_items> cartMap = new HashMap();
        cartMap.put(foodID, new Order_items(foodID, food_amount, orderID));

        return cartMap;
    }

    public Food getFoodByID(String foodID) {
        Food food = null;
        try {
            String query = "SELECT * FROM `Food` WHERE `food_id` LIKE ?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, foodID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                food = new Food(rs.getString("food_id"), rs.getString("food_name"), rs.getString("food_image"), rs.getString("food_desc"), rs.getString("food_status"), rs.getFloat("food_price"), rs.getString("category_id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return food;
    }

    public int updateFoodAmount(int foodAmount, String foodID, String orderID) {
        int count = 0;
        try {
            PreparedStatement pst = null;
            String query = "UPDATE `Order_items` SET `food_amount` = ? WHERE `Order_items`.`food_id` = ? AND `Order_items`.`order_id` = ?";
            pst = connection.prepareStatement(query);
            pst.setInt(1, foodAmount);
            pst.setString(2, foodID);
            pst.setString(3, orderID);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TableDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public String getNameByCatID(String categoryID) {
        String name = "";
        try {
            String query = "SELECT Category.category_name FROM Category WHERE category_id LIKE ? ";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, categoryID);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()) {
                name = resultSet.getString("category_name");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return name;
    }

    public boolean isExistsFoodID(String foodID, String orderID) {
        boolean isExist = false;
        Food food = null;

        try {
            String query = "SELECT Order_items.food_id FROM `Order_items` WHERE Order_items.food_id LIKE ? AND Order_items.order_id = ?";

            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, foodID);
            pst.setString(2, orderID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                food = new Food(rs.getString("food_id"));
                if (foodID.equalsIgnoreCase(food.getFood_id())) {
                    isExist = true;
                    return isExist;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isExist;
    }

}
