package com.DAOS;

import com.DBConnection.DBConnection;
import com.models.Food;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hung
 */
public class MenuManageDAO {

    private final Connection conn;

    public MenuManageDAO() {
        conn = DBConnection.getConnection();
    }

    public ResultSet getAllFood() {
        ResultSet rs = null;
        try {
            Statement st = conn.createStatement();
            rs = st.executeQuery("Select * from food where food_status='Available' OR food_status='Expired'");
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getAllCategory() {
        ResultSet rs = null;
        try {
            Statement st = conn.createStatement();
            rs = st.executeQuery("Select * from category");
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public int addFood(Food f) {
        int count = 0;
        try {
            PreparedStatement st = conn.prepareCall("SELECT food_id FROM `food` WHERE food_id LIKE ?");
            st.setString(1, f.food_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString("food_id").equals(f.food_id)) {
                    String[] part = f.food_id.split("(?<=\\D)(?=\\d)");
                    f.food_id = "f" + String.valueOf(Integer.parseInt(part[1]) + 1);
                }
            }

            PreparedStatement pst = conn.prepareCall("Insert into food values(?, ?, ?, ?, ?, ?, ?)");
            pst.setString(1, f.food_id);
            pst.setString(2, f.food_name);
            pst.setString(3, f.food_image);
            pst.setString(4, f.food_desc);
            pst.setString(5, f.food_status);
            pst.setFloat(6, f.food_price);
            pst.setString(7, f.category_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int updateFood(Food f) {
        int count = 0;
        try {
            PreparedStatement pst = conn.prepareCall("Update food set food_name=?,food_image=?,food_desc=?,food_status=?,food_price=?,category_id=? where food_id=?");
            pst.setString(1, f.food_name);
            pst.setString(2, f.food_image);
            pst.setString(3, f.food_desc);
            pst.setString(4, f.food_status);
            pst.setFloat(5, f.food_price);
            pst.setString(6, f.category_id);
            pst.setString(7, f.food_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int deleteFood(String f_id) {
        int count = 0;
        try {
            PreparedStatement pst = conn.prepareCall("Update food set food_status='Deleted' where food_id=?");
            pst.setString(1, f_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public boolean checkFoodName(String f_name) {
        boolean c = true;
        try {
            PreparedStatement pst = conn.prepareCall("SELECT * FROM `food` WHERE food_name LIKE ?");
            pst.setString(1, f_name);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getString("food_name").equalsIgnoreCase(f_name)) {
                    c = false;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
}
