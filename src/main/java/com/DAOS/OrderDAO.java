package com.DAOS;

import com.DBConnection.DBConnection;
import com.models.Category;
import com.models.Food;
import com.models.Invoice;
import com.models._Order;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cuongseven
 */
public class OrderDAO {

    private Connection connection = null;

    public OrderDAO() {
        connection = DBConnection.getConnection();
    }

    public int addNewOrder(_Order order) {

        String query = "INSERT INTO `_Order` (`order_id`, `order_status`, `order_tableNumber`, `order_tablePeople`, `order_tableDate`, `order_tableTimeCheckin`, `order_tableNote`, `invoice_id`, `user_id`, `table_id`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        int count = 0;
        try ( PreparedStatement pst = connection.prepareStatement(query)) {

            pst.setString(1, order.getOrder_id());
            pst.setString(2, order.getOrder_status());
            pst.setInt(3, order.getOrder_tableNumber());
            pst.setInt(4, order.getOrder_tablePeople());
            pst.setString(5, order.getOrder_tableDate());
            pst.setString(6, order.getOrder_tableTimeCheckin());
            pst.setString(7, order.getOrder_tableNote());
            pst.setString(8, order.getInvoice_id());
            pst.setString(9, order.getUser_id());
            pst.setString(10, order.getTable_id());
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;

    }

    public _Order getOrderByID(String id) {
        _Order order = null;
        try {
            String query = "SELECT * FROM `_Order` WHERE `order_id` LIKE ?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                order = new _Order(rs.getString("order_id"), rs.getString("order_status"), rs.getInt("order_tableNumber"), rs.getInt("order_tablePeople"), rs.getString("order_tableDate"), rs.getString("order_tableTimeCheckin"), rs.getString("order_tableNote"), rs.getString("invoice_id"), rs.getString("user_id"), rs.getString("table_id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

    // ========== new ========= //
    public ResultSet getOrderByUserID(String user_id) {
        ResultSet rs = null;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM _Order WHERE user_id=?");
            pst.setString(1, user_id);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public Invoice getInvoiceById(String invoice_id) {
        Invoice st = null;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM Invoice WHERE invoice_id=?");
            pst.setString(1, invoice_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                st = new Invoice(rs.getString("invoice_id"), rs.getFloat("invoice_total"), rs.getString("invoice_timePayment"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return st;
    }

    public int getNumberOfInvoice() {

        try {
            Statement st = null;
            ResultSet rs = null;
            String query = "SELECT COUNT(Invoice.invoice_id) as 'i' FROM `Invoice`";
            st = connection.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                return rs.getInt("i");
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public boolean isExistsInvoiceID(String invoice_id) {
        boolean isExist = false;
        Invoice invoice = null;

        try {
            String query = "SELECT Invoice.invoice_id FROM Invoice WHERE Invoice.invoice_id LIKE ?";

            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, invoice_id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                invoice = new Invoice(rs.getString("invoice_id"));
                if (invoice_id.equalsIgnoreCase(invoice.getInvoice_id())) {
                    isExist = true;
                    return isExist;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isExist;
    }

    public ResultSet getFoodBookedByOrderID(String order_id) {
        ResultSet rs = null;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM Order_items WHERE order_id=?");
            pst.setString(1, order_id);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public Food getFoodById(String food_id) {
        Food st = null;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM Food WHERE food_id=?");
            pst.setString(1, food_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                st = new Food(rs.getString("food_id"),
                        rs.getString("food_name"),
                        rs.getString("food_image"),
                        rs.getString("food_desc"),
                        rs.getString("food_status"),
                        rs.getFloat("food_price"),
                        rs.getString("category_id")
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return st;
    }

    public Category getCategoryById(String category_id) {
        Category st = null;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM Category WHERE category_id=?");
            pst.setString(1, category_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                st = new Category(rs.getString("category_id"),
                        rs.getString("category_name")
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return st;
    }
    // ========== new ========= //

    public boolean isExistsOrderID(String orderID) {
        boolean isExist = false;
        _Order order = null;

        try {
            String query = "SELECT _Order.order_id FROM `_Order` WHERE _Order.order_id LIKE ?";

            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, orderID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                order = new _Order(rs.getString("order_id"));
                if (orderID.equalsIgnoreCase(order.getOrder_id())) {
                    isExist = true;
                    return isExist;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isExist;
    }

    public int getNumberOfOrder() {

        try {
            Statement st = null;
            ResultSet rs = null;
            String query = "SELECT COUNT(order_id) as 'o' FROM `_Order`";
            st = connection.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                return rs.getInt("o");
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int addNewInvoice(Invoice invoice) {
        String query = "INSERT INTO `Invoice` (`invoice_id`, `invoice_total`, `invoice_timePayment`) VALUES (?, ?, ?)";
        int count = 0;

        try ( PreparedStatement pst = connection.prepareStatement(query)) {

            pst.setString(1, invoice.getInvoice_id());
            pst.setFloat(2, invoice.getInvoice_total());
            pst.setString(3, invoice.getInvoice_timePayment());
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public float updateInvoiceTotal(float invoice_total, String invoiceID) {
        int count = 0;
        try {
            PreparedStatement pst = null;
            String query = "UPDATE `Invoice` SET `invoice_total`= ? WHERE Invoice.invoice_id = ?";

            pst = connection.prepareStatement(query);
            pst.setFloat(1, invoice_total);
            pst.setString(2, invoiceID);

            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int deleteOrderItemsByOID(String orderID) {
        int count = 0;
        try {
            PreparedStatement pst = null;
            String query = "DELETE FROM `Order_items` WHERE Order_items.order_id LIKE ?";
            pst = connection.prepareStatement(query);
            pst.setString(1, orderID);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int deleteOrderByID(String orderID) {
        int count = 0;
        try {
            PreparedStatement pst = connection.prepareCall("DELETE FROM `_Order` WHERE _Order.order_id LIKE ?");
            pst.setString(1, orderID);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int deleteInvoiceByID(String invoiceID) {
        int count = 0;
        try {
            PreparedStatement pst = null;
            String query = "DELETE FROM `Invoice` WHERE Invoice.invoice_id LIKE ?";

            pst = connection.prepareStatement(query);
            pst.setString(1, invoiceID);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }
}
