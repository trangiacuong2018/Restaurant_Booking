/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.DAOS;

import com.DBConnection.DBConnection;
import com.models._Table;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hung
 */
public class OrderManageDAO {

    private final Connection conn;

    public OrderManageDAO() {
        conn = DBConnection.getConnection();
    }

    public ResultSet getAllOrder() {
        ResultSet rs = null;
        try {
            Statement st = conn.createStatement();
            rs = st.executeQuery("SELECT _order.*,invoice.invoice_total,invoice.invoice_timePayment,"
                    + "_user.user_fullName,_user.user_phone FROM _user,_order,invoice "
                    + "WHERE _order.user_id = _user.user_id AND _order.invoice_id = invoice.invoice_id "
                    + "AND _order.order_status = 'Processing'");
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getItemsByOID(String o_id) {
        ResultSet rs = null;
        try {
            PreparedStatement pst = conn.prepareStatement("SELECT food.food_name, order_items.* "
                    + "FROM food,order_items WHERE food.food_id = order_items.food_id AND order_items.order_id= ?");
            pst.setString(1, o_id);
            rs = pst.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public String getTableNumberByOID(String o_id) {
        ResultSet rs = null;
        String t = "";
        try {
            PreparedStatement pst = conn.prepareStatement("SELECT order_tableNumber FROM _order WHERE order_id= ?");
            pst.setString(1, o_id);
            rs = pst.executeQuery();
            while (rs.next()) {
                t = rs.getString("order_tableNumber");
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
    }

    public int changeTableStatus(String t_id) {
        int count = 0;
        try {
            PreparedStatement pst = conn.prepareCall("UPDATE _table set table_status=? WHERE table_id=?");
            String status = "";
            TableDAO t = new TableDAO();
            _Table table = t.getTableByID(t_id);
            if (table.getTable_status().equals("Available")) {
                status = "Busy";
            } else {
                status = "Available";
            }
            pst.setString(1, status);
            pst.setString(2, t_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int changeOrderStatus(String o_id) {
        int count = 0;
        try {
            PreparedStatement pst = conn.prepareCall("UPDATE _order set order_status='Confirmed' WHERE order_id=?");
            pst.setString(1, o_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int deleteOrder(String o_id) {
        int count = 0;
        try {
            PreparedStatement pst = conn.prepareCall("UPDATE _order set order_status='Canceled' WHERE order_id=?");
            pst.setString(1, o_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderManageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }
}
