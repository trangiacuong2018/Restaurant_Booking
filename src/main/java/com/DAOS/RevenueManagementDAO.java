/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.DAOS;

import com.DBConnection.DBConnection;
import com.models._User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class RevenueManagementDAO {

    private Connection conn;

    public RevenueManagementDAO() {
        conn = DBConnection.getConnection();
    }

    public ResultSet getAllRevenueByDate(String min, String max) {
        ResultSet set = null;

        String query = "SELECT * FROM `invoice` "
                + "LEFT OUTER JOIN `_order` "
                + "ON `_order`.`invoice_id` = `invoice`.`invoice_id` "
                + "LEFT OUTER JOIN `_table` ON `_table`.`table_id` = `_order`.`table_id` "
                + "WHERE `invoice`.`invoice_timePayment` >=? "
                + "AND `invoice`.`invoice_timePayment` <= DATE_ADD(?, INTERVAL 1 DAY) "
                + "AND _order.order_status = 'Confirmed'";
        try {
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, min);
            pst.setString(2, max);
            set = pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(RevenueManagementDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return set;
    }

    public ResultSet getAllRevenue() {
        ResultSet set = null;

        String query = "SELECT * FROM `invoice` "
                + "LEFT OUTER JOIN `_order` "
                + "ON `_order`.`invoice_id` = `invoice`.`invoice_id` "
                + "LEFT OUTER JOIN `_table` ON `_table`.`table_id` = `_order`.`table_id` "
                + "WHERE `_order`.`order_status` = 'Confirmed'";
        try {
            PreparedStatement pst = conn.prepareStatement(query);
            set = pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(RevenueManagementDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return set;
    }

     public _User getUserByID(String user_id){
        _User user = null;
        
        String query = "SELECT * FROM _User WHERE _User.user_id = ?";
        
        try {
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, user_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                user = new _User(rs.getString("user_id"), rs.getString("user_fullName"), rs.getString("email"), 
                        rs.getString("user_role"), rs.getString("user_phone"), 
                        rs.getString("username"), rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RevenueManagementDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return user;
        
    }



}
