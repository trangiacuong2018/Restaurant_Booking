/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.DAOS;

import com.DBConnection.DBConnection;
import com.Filters.MD5Hash;
import com.models._User;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class UserDAO {

    private Connection connection = null;

    public UserDAO() {
        connection = DBConnection.getConnection();
    }

    public ResultSet getAllUser() {
        ResultSet resultSet = null;
        String query = "SELECT * FROM `_user`";
        try {
            PreparedStatement pst = connection.prepareStatement(query);
            resultSet = pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultSet;
    }

    public int deleteUser(String user_id) {
        int count = 0;
        String query = "UPDATE `_user` SET `user_role`='delete' WHERE user_id=?";
        try {
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, user_id);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int updateUser(_User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        int count = 0;

        String query = "UPDATE `_user` SET "
                + "`user_fullName`=?, `email`=?, `user_phone`=?,`password`=?"
                + " WHERE `user_id`=?";
        String passHash = new MD5Hash().encrypt(user.getPassword());
        try {
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, user.getUser_fullName());
            pst.setString(2, user.getEmail());
            pst.setString(3, user.getUser_phone());
            pst.setString(4, passHash);
            pst.setString(5, user.getUser_id());
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public int addUser(_User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        int count = 0;

        String query = "INSERT INTO `_user` (`user_id`, `user_fullName`, `email`, "
                + "`user_role`, `user_phone`, `username`, `password`) VALUES (?,?,?,?,?,?,?);";
        String passHash = new MD5Hash().encrypt(user.getPassword());
        try {
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, user.getUser_id());
            pst.setString(2, user.getUser_fullName());
            pst.setString(3, user.getEmail());
            pst.setString(4, user.getUser_role());
            pst.setString(5, user.getUser_phone());
            pst.setString(6, user.getUsername());
            pst.setString(7, passHash);
            count = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public boolean checkIDUser(String id) {
        String query = "SELECT * FROM `_user` WHERE `user_id`=?";

        try {
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString("user_id").equals(id)) {
                    return false;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public boolean checkUserName(String username) {
        String query = "SELECT * FROM `_user` WHERE `username`=?";

        try {
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, username);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()) {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
    public boolean checkEmail(String email) {
        String query = "SELECT * FROM `_user` WHERE `email`=?";

        try {
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, email);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()) {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
