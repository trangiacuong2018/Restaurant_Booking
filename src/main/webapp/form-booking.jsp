

<%@page import="com.DAOS.AccountDAO"%>
<%@page import="com.models._User"%>
<%@page import="com.models._Order"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.LocalTime"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="com.models._Table"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <link rel="stylesheet" href="resouces/css/homepage-styles.css"/>

        <title>Booking Form</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <%@include file="import-css-links.jsp"%>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resouces/js/menu-js.js" />
        <style>
            .disbale {
                display: none;
            }

            .error {
                color: red;
            }
        </style>
    </head>

    <body class="container-fluid" style="margin-top: 100px;">
        <%@include file="header.jsp" %>
        <%
            if (!account.equals("")) {
                if (accDAO.getAccountById(account).getUser_role().equals("staff")) {
                    response.sendRedirect(request.getContextPath() + "/orders-management");
                } else if (accDAO.getAccountById(account).getUser_role().equals("admin")) {
                    response.sendRedirect(request.getContextPath() + "/revenue-management");
                }
            }
            //GET useID from cookie. setted at menu-management page.
            cookies = request.getCookies();
            String userID = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("account")) {
                        userID = cookie.getValue();
                        break;
                    }
                }
            }
            AccountDAO accountDAO = new AccountDAO();
            //get userID object to pass into (fullname, email,phone).
            _User user = accountDAO.getAccountById(userID);
            if (user != null) {


        %>
        <!--Booking a table-->
        <section class="container "style="font-size: 15px">
            <form name="Form" action="/order/" method="POST" onsubmit="handleValidation()">
                <div class="form-group p-lg-4 my-3 rounded-4" style="background-color: #FFFFCC;border: 1px solid black;padding: 10px">
                    <h1 class="mb-4 text-center" style="font-family: 'Shantell Sans', cursive;">Booking information</h1>


                    <div class="row mb-4">

                        <!--Date Input-->
                        <div class="col-3">
                            <div class="form-outline">
                                <label class="form-label" >Date</label>
                                <input type="date" id="myDate" name="txtDate"  class="form-control" required="" min=""/>
                                <span id="span-date" class="disbale"><p class="error">You must choice date!</p></span>
                            </div>

                            <!-- create input USerID used to Pass it to OrderController -->
                            <input type="text" name="txtUserID" value="<%=userID%>"  class="form-control" hidden=""/>
                        </div>

                        <!--Time Check-in input-->
                        <div class="col">
                            <div class="form-outline">
                                <label class="form-label">Time Check-in</label>
                                <input type="time" id="myTime" name="txtTime" class="form-control" required="" placeholder="Example: 4:20 AM"/>
                                <span id="span-time" class="disbale"><p id="error-ms" class="error">You must choice time checkin!</p></span>
                            </div>
                        </div>
                        <!-- People input -->
                        <div class="col">
                            <div class="form-outline">
                                <label class="form-label">People</label>
                                <input type="number" id="myPeople" name="txtPeople" value="" class="form-control" placeholder="Please enter a number"/>
                                <span id="span-people" class="disbale"><p class="error">You must choice from 1 to 20 people</p></span>
                            </div>
                        </div>

                    </div>

                    <!-- Email input -->
                    <div class="row mb-4">

                        <div class="col">
                            <div class="form-outline">
                                <label class="form-label">Full name</label>
                                <input type="text" id="myFullname" name="txtFullName" value="<%= user.getUser_fullName()%>" class="form-control" />
                                <span id="span-fn" class="disbale"><p class="error">Fullname can not be empty and less than 100 characters</p></span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <label class="form-label">Email</label>
                                <input type="email" id="myEmail" name="txtEmail" class="form-control" value="<%=user.getEmail()%>" readonly=""/>
                                <span id="span-email" class="disbale"><p class="error">Email can not be empty</p></span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <label class="form-label">Phone</label>
                                <input type="text" id="myPhone" name="txtPhone" value="<%= user.getUser_phone()%>" class="form-control"/>
                                <span id="span-phone" class="disbale"><p class="error">Your phone must be 10 digits</p></span>
                            </div>
                        </div>

                    </div>

                    <!-- Note input -->
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form6Example7">Note</label>
                        <textarea class="form-control"  id="form6Example7" name="txtNote" rows="4" cols="8" placeholder="Please enter your note here !" style="font-size: 20px"></textarea>
                    </div>

                    <!-- Submit button -->
                    <div class="form-outline mb-4 d-flex justify-content-center">
                        <button type="submit"
                                name="btnSubmit"
                                onclick="return check()"
                                class="btn btn-block mb-4" 
                                style="background-color: black;width: 20%">
                            <h3 style="color: white;">Choose Dishes</h3>
                        </button>
                    </div>

                </div>

            </form>
        </section>
        <%@include file="footer.jsp" %>
        <%} //end IF
        else {%> 
        <%@include file="error.jsp" %>
        <%            }
        %> 

        <!-- Bootstrap JS Library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
                                    const today = new Date().toISOString().split('T')[0];
                                    document.getElementById("myDate").min = today;

                                    const startTime = '07:00';
                                    const endTime = '23:00';

                                    function check() {
                                        var myDate = document.forms["Form"]["myDate"].value;
                                        var myTime = document.forms["Form"]["myTime"].value;
                                        var myPeople = document.forms["Form"]["myPeople"].value;
                                        var myEmail = document.forms["Form"]["myEmail"].value;
                                        var myPhone = document.forms["Form"]["myPhone"].value;
                                        var myFullname = document.forms["Form"]["myFullname"].value;

                                        if (myDate === null || myDate === "") {
                                            document.getElementById("span-date").classList.remove("disbale");
                                            return false;
                                        } else {
                                            document.getElementById("span-date").classList.add("disbale");
                                        }

                                        if (myTime === null || myTime === "") {
                                            document.getElementById("span-time").classList.remove("disbale");
                                            document.getElementById("error-ms").innerText = "You must choice time checkin!";
                                            return false;
                                        } else {
                                            document.getElementById("span-time").classList.add("disbale");
                                        }

                                        if (!checkChoiceTimeInFuture(myDate, myTime)) {
                                            document.getElementById("span-time").classList.remove("disbale");
                                            document.getElementById("error-ms").innerText = "You must be choice date time in future";
                                            return false;
                                        } else {
                                            document.getElementById("span-time").classList.add("disbale");
                                        }

                                        if (!checkChoiceTimeInOffice(myTime)) {
                                            document.getElementById("span-time").classList.remove("disbale");
                                            document.getElementById("error-ms").innerText = "You must choice time checkin in office time";
                                            return false;
                                        } else {
                                            document.getElementById("span-time").classList.add("disbale");
                                        }

                                        if (checkCurrentDay(myDate)) {
                                            if (!checkChoiceTimeRange(myTime)) {
                                                document.getElementById("span-time").classList.remove("disbale");
                                                document.getElementById("error-ms").innerText = "You must choice time checkin greater than 2 hours with current time";
                                                return false;
                                            } else {
                                                document.getElementById("span-time").classList.add("disbale");
                                            }
                                        }

                                        if (myPeople === null || myPeople < 1 || myPeople > 20) {
                                            document.getElementById("span-people").classList.remove("disbale");
                                            return false;
                                        } else {
                                            document.getElementById("span-people").classList.add("disbale");
                                        }
                                        if (myFullname === null || myFullname === "" || myFullname.length > 100) {
                                            document.getElementById("span-fn").classList.remove("disbale");
                                            return false;
                                        } else {
                                            document.getElementById("span-fn").classList.add("disbale");
                                        }
                                        if (myEmail === null || myEmail === "") {
                                            document.getElementById("span-email").classList.remove("disbale");
                                            return false;
                                        } else {
                                            document.getElementById("span-email").classList.add("disbale");
                                        }
                                        if (regexPhoneNumber(myPhone) === false) {
                                            document.getElementById("span-phone").classList.remove("disbale");
                                            return false;
                                        } else {
                                            document.getElementById("span-phone").classList.add("disbale");
                                        }
                                    }

                                    function checkChoiceTimeInFuture(myDate, myTime) {
                                        const currentDate = new Date();
                                        const year = currentDate.getFullYear();
                                        const month = currentDate.getMonth() + 1;
                                        const day = currentDate.getDate();
                                        const correctCurrentDay = year + '-0' + month + '-' + day;

                                        currentDate.setSeconds(0);
                                        const currentTime = currentDate.toLocaleTimeString('en-US', {hour12: false});

                                        console.log(currentTime);
                                        console.log(myTime + ":00");

                                        if (correctCurrentDay.toString() === myDate.toString()) {
                                            if (currentTime < myTime) {
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            return true;
                                        }
                                    }

                                    function checkCurrentDay(myDate) {
                                        const currentDate = new Date();
                                        const year = currentDate.getFullYear();
                                        const month = currentDate.getMonth() + 1;
                                        const day = currentDate.getDate();
                                        const correctCurrentDay = year + '-0' + month + '-' + day;

                                        if (correctCurrentDay.toString() === myDate.toString()) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }

                                    function checkChoiceTimeInOffice(myTime) {
                                        const inputHours = parseInt(myTime.substring(0, 2));
                                        const inputMinutes = parseInt(myTime.substring(3, 5));

                                        const startHours = parseInt(startTime.substring(0, 2));
                                        const startMinutes = parseInt(startTime.substring(3, 5));

                                        const endHours = parseInt(endTime.substring(0, 2));
                                        const endMinutes = parseInt(endTime.substring(3, 5));

                                        if ((inputHours > startHours && inputHours < endHours) || (inputHours === startHours && inputMinutes >= startMinutes) || (inputHours === endHours && inputMinutes <= endMinutes)) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }

                                    function checkChoiceTimeRange(myTime) {
                                        const inputDate = new Date(); // today's date
                                        const inputHours = parseInt(myTime.substring(0, 2));
                                        const inputMinutes = parseInt(myTime.substring(3, 5));
                                        inputDate.setHours(inputHours, inputMinutes); // set the input time to today's date

                                        const currentDate = new Date();
                                        const diffInMs = inputDate - currentDate;
                                        const diffInHours = diffInMs / (1000 * 60 * 60);

                                        console.log(inputHours, inputMinutes);
                                        console.log(diffInMs, diffInHours);

                                        if (diffInHours >= 2) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }

                                    function checkCurrentTimeInOffice() {
                                        const currentDate = new Date();
                                        const currentHours = currentDate.getHours();
                                        const currentMinutes = currentDate.getMinutes();
                                        const currentTime = currentHours.toString().padStart(2, '0') + ':' + currentMinutes.toString().padStart(2, '0');

                                        if ((currentTime > startTime && currentTime < endTime) || currentTime === startTime || currentTime === endTime) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }

                                    function regexPhoneNumber(phone) {
                                        const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
                                        return phone.match(regexPhoneNumber) ? true : false;
                                    }
        </script>

    </body>

</html>

