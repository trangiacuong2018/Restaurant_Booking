<%-- 
    Document   : payment
    Created on : Feb 20, 2023, 2:47:30 PM
    Author     : nghiempt
--%>

<%@page import="com.models._User"%>
<%@page import="com.models.Order_items"%>
<%@page import="com.models.Food"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.DAOS.FoodDAO"%>
<%@page import="com.DAOS.OrderDAO"%>
<%@page import="com.DAOS.TableDAO"%>
<%@page import="com.models._Table"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

    <head>
        <!-- Title Page -->
        <title>Payment Page</title>

        <!-- Bootstrap JS Library -->
        <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css">-->
        <%@include file="import-css-links.jsp" %>
        <%@include file="import-js-links.jsp" %>

        <!-- CSS -->
        <style>
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }

            #payment_app {
                width: 100%;
                height: 100vh;
                display: flex;
                justify-content: start;
                align-items: start;
                flex-direction: column;
            }

            .payment_btn_back {
                margin-top: 100px;
                margin-left: 40px;
                margin-bottom: 10px;
                padding: 5px 20px;
            }

            .payment_title {
                background-color: #e82d20;
                width: 90%;
                display: flex;
                justify-content: space-between;
                align-items: center;
                padding: 10px 40px;
                margin: 10px 40px;
            }

            .payment_info {
                display: flex;
                background-color: #ccc;
                width: 90%;
                padding: 10px 40px;
                margin: 10px 40px;
                flex-wrap: wrap;
            }

            .payment_info_item {
                width: 30%;
                padding: 10px 0;
            }

            .payment_table {
                width: 90%;
                margin: 10px 40px;
            }

            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

            .payment_detail {
                display: flex;
                background-color: #ccc;
                width: 90%;
                padding: 10px 40px;
                margin: 10px 40px;
                flex-wrap: wrap;
            }

            .payment_detail_item {
                width: 40%;
                padding: 10px 0;
            }

            .payment_confirm {
                width: 90%;
                margin: 10px 40px;
                display: flex;
                justify-content: space-evenly;
            }

            .payment_footer {
                width: 90%;
                margin: 10px 40px;
                display: flex;
                justify-content: space-evenly;
            }

            .payment_btn_cancel {
                padding: 10px 50px;
                background-color: #C95C54;
                color: white;
                border: none;
                border-radius: 5px;
            }

            .payment_btn_submit {
                padding: 10px 50px;
                background-color: green;
                color: white;
                border: none;
                border-radius: 5px;
            }

            .modal-header {
                background: crimson;
                color: #fff;
            }

            .required:after {
                content: "*";
                color: red;
            }
        </style>
    </head>

    <%@include file="header.jsp" %>
    <body>
        <%
            TableDAO tableDAO = new TableDAO();
            OrderDAO orderDAO = new OrderDAO();
            FoodDAO FoodDAO = new FoodDAO();

            //Lấy số bàn dựa vào TableID
            String tableID = session.getAttribute("tableID").toString();
            _Table table = tableDAO.getTableByID(tableID);
            int table_number = table.getTable_number();

            //Lấy data từ session
            String fullname = session.getAttribute("fullName").toString();
            String phone = session.getAttribute("phone").toString();
            String orderID = session.getAttribute("orderID").toString();
            String tableDate = session.getAttribute("table_date").toString();
            String timeCheckin = session.getAttribute("timeCheckin").toString();
            String people = session.getAttribute("people_number").toString();
            String note = session.getAttribute("note").toString();
            String InvoiceID_number = session.getAttribute("InvoiceID_number").toString();
            String totalPayment = session.getAttribute("totalPayment").toString();
        %>

        <!-- Payment App -->
        <div class="container" id="payment_app ">

            <!-- Back Button -->
            <section class="container-fluid" style="margin-top:130px">
                <div class="d-flex align-items-end flex-column" style="margin-right: 15rem">
                    <a href="/cart">
                            <svg xmlns="http://www.w3.org/2000/svg" width="160" height="160" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16" style="font-size: 40px">
                            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                            </svg>
                    </a>                    
                </div>
                <h1 class="text-center"  style="font-family: 'Shantell Sans', cursive;font-size: 50px">Invoice</h1>
            </section>
            <section class="container-fluid">
                <!-- Payment Title -->
                <div class="payment_title">
                    <h1>INVOICE</h1>
                    <h1><b>No:</b> <%=orderID%></h1>
                </div>

                <!-- Payment Info -->
                <div class="payment_info">
                    <div class="payment_info_item">
                        <p><b>Customer Name:</b> <%=fullname%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>Customer Phone</b> <%=phone%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>Time check-in:</b> <%=timeCheckin%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>People: </b> <%=people%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>Date:</b> <%=tableDate%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>Table:</b> <%=table_number%></p>
                    </div>
                    <div class="payment_info_item">
                        <p><b>Invoice number:</b> <%=InvoiceID_number%></p>
                    </div>
                </div>

                <!-- Payment Table -->

                <div class="payment_table">

                    <table>
                        <tr>
                            <th>No</th>
                            <th>Food name</th>
                            <th>Quantity</th>
                            <th>Unit price</th>
                            <th>Total</th>
                        </tr>
                        <%
                            HashMap<String, Integer> cartMenu = (HashMap<String, Integer>) session.getAttribute("cartMenu");

                            for (Map.Entry<String, Integer> entry : cartMenu.entrySet()) {

                                String foodID = entry.getKey();
                                int food_amount = entry.getValue();

                                Food f = FoodDAO.getFoodByID(foodID);
//                                String category_name = FoodDAO.getNameByCatID(f.getCategory_id());
                                float totalEachFoodPrice = food_amount * f.getFood_price();
                        %>
                        <tr>
                            <td>01</td>
                            <td><%=f.getFood_name()%></td>
                            <td><%=food_amount%></td>
                            <td><%=f.getFood_price()%></td>
                            <td><%=totalEachFoodPrice%></td>
                        </tr>
                        <%
                            }
                        %>
                    </table>

                </div>


                <!-- Payment Detail -->
                <div class="payment_detail">
                    <div class="payment_detail_item">
                        <p>Number of deposit in advance:</p>
                    </div>
                    <div class="payment_detail_item">
                        <p><b>Phone number of Staff: </b><a href="facetime:0845296917" onclick="openApp()">0845.2969.17</a></p>
                    </div>
                    <div class="payment_detail_item">
                        <p>Discount:</p>
                    </div>
                    <div class="payment_detail_item">
                        <h3>Total paid: $<%=totalPayment%></h3>
                    </div>
                </div>
            </section>
            <!-- Payment Confirm -->

            <div class="payment_confirm">
                <form action="/payment"method="POST">
                    <button type="submit" name="btnCancel" class="payment_btn_cancel">Cancel</button>
                    <button type="submit" name="btnPayment" class="payment_btn_submit">Payment</button>
                </form>

            </div>

            <!-- Payment Footer -->
            <div class="payment_footer">
                <p>Thank you. See you soon</p>
            </div>

        </div>
        <script>

            function openApp() {
                // Kiểm tra xem trình duyệt hiện tại là Safari hay không
                if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1 && navigator.userAgent.indexOf('Version/') != -1) {
                    // Safari trên Macbook, chuyển đổi sang chế độ mở ứng dụng
                    window.location.href = "facetime:0123456789";
                    setTimeout(function () {
                        // Đợi 1 giây để chuyển sang chế độ mở ứng dụng
                        window.location.href = "facetime:0123456789";
                    }, 1000);
                } else {
                    // Trình duyệt không hỗ trợ chuyển sang chế độ mở ứng dụng, chuyển đến trang facetime:
                    window.location.href = "facetime:0123456789";
                }
            }
        </script>

    </body>

    <%@include file="footer.jsp" %>
</html>