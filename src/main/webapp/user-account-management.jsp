
<%@page import="com.DAOS.AccountDAO"%>
<%@page import="java.util.concurrent.atomic.AtomicInteger"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.DAOS.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>User Management</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <%@include file="import-css-links.jsp"%>
        <style>
            .modal-header {
                background: crimson;
                color: #fff;
            }

            .required:after {
                content: "*";
                color: red;
            }
        </style>
    </head>


    <body>
        <!-- ======= Header ======= -->
        <%@include file= "header-admin.jsp" %>
        <%
            cookies = request.getCookies();
            String accountId = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("account")) {
                        accountId = cookie.getValue();
                        break;
                    }
                }
            }
            if (account.equals("")) {
                response.sendRedirect(request.getContextPath() + "/");
            } else {
                AccountDAO acc = new AccountDAO();
                if (acc.getAccountById(accountId).getUser_role().equals("admin")) {
        %>
        <!-- End Header -->
        <div style="margin-top: 50px" class="container  ">
            <div class="container my-5 pt-5 pb-4 d-flex flex-row-reverse ">
                <div class="btn-group btn-group-lg ">
                    <a href="<%= request.getContextPath()%>/staff-management"> <button type="button" class="btn btn-danger">Staff account management</button></a>
                </div>
                <div class="btn-group btn-group-lg ">
                    <button type="button" class="btn "></button>
                </div>
                <div class="btn-group btn-group-lg ">
                    <a href="/revenue-management"><button type="button" class="btn btn-danger">Revenue management</button></a>
                </div> <div class="btn-group btn-group-lg ">
                    <button type="button" class="btn "></button>
                </div>
            </div>
        </div>
        <!--Modal Delete-->
        <!-- About Start -->
        <div class="container-fluid py-5">
            <h1 class="text-center">User Account Management</h1>
            <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Full name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Update</th>
                    </tr>
                </thead>
                <tbody>

                    <%                        UserDAO userDAO = new UserDAO();
                        ResultSet rs = userDAO.getAllUser();
                        AtomicInteger count = new AtomicInteger(1);
                        while (rs.next()) {
                            if (rs.getString("user_role").equals("user")) {
                    %>
                    <tr>
                        <td><%= count.getAndIncrement()%></td>
                        <td><%= rs.getString("user_fullName")%></td>
                        <td><%= rs.getString("email")%></td>
                        <td><%= rs.getString("user_phone")%></td>
                        <td><%= rs.getString("username")%></td>
                        <td><%= rs.getString("password")%></td>
                        <td>
                            <div class="row">

                                <!--End update-->
                                <div class="col-6">
                                    <form action="user-management" method="post">
                                        <input type="hidden" name="user_id" value="<%= rs.getString("user_id")%>">
                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#Delete-<%= rs.getString("user_id")%>">Delete</button>

                                        <div class="modal" id="Delete-<%= rs.getString("user_id")%>">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Confirm delete</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                                    </div>
                                                    <div class="modal-body text-start">
                                                        <div class="text-center"><h4>Do you want to delete this?</h4></div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" name="confirmDelete" class="btn btn-primary" id="confirmDelete">Yes</button></a>
                                                        <a href="/user-management"><button type="button" class="btn btn-danger">No</button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <%
                            }
                        }
                    %>
                </tbody>
            </table>
        </div>
        <!-- About End -->

        <%
                } else if (acc.getAccountById(accountId).getUser_role().equals("staff")) {
                    response.sendRedirect(request.getContextPath() + "/orders-management");
                } else if (acc.getAccountById(accountId).getUser_role().equals("user")) {
                    response.sendRedirect(request.getContextPath() + "/");
                }
            }
        %>




    </div>

    <a href="#" class="scroll-top d-flex align-items-center
       justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <%@include file="import-js-links.jsp" %>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap5.min.js"></script>
</body>
</html>





