<%-- 
    Document   : menu
    Created on : Feb 18, 2023, 7:06:21 AM
    Author     : giacu
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.models.Order_items"%>
<%@page import="com.DAOS.FoodDAO"%>
<%@page import="com.models.Food"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="resouces/js/handleIn-decrease-btn.js"></script>
        <title>Restaurant Booking</title>

        <%@include file="import-css-links.jsp" %>
        <%@include file="import-js-links.jsp" %>

        <style>
            * {
                box-sizing: border-box;
            }

            body {
                background-color: #f1f1f1;
                padding: 20px;
                font-family: Arial;
            }

            /* Center website */
            .main {
                max-width: 1000px;
                margin: auto;
            }

            h1 {
                font-size: 50px;
                word-break: break-all;
            }

            .row {
                margin: 10px -16px;
            }

            /* Add padding BETWEEN each column */
            .row,
            .row > .column {
                padding: 8px;
            }

            /* Create three equal columns that floats next to each other */
            .column {
                float: left;
                width: 33.33%;
                display: none; /* Hide all elements by default */
            }

            /* Clear floats after rows */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Content */
            .content {
                background-color: rgba(212, 200, 200, 0.57);
                padding: 10px;
                border-radius: 7px;
            }

            /* The "show" class is added to the filtered elements */
            .show {
                display: block;
            }

            /* Style the buttons */
            .btn {
                border: none;
                outline: none;
                padding: 12px 16px;
                background-color: white;
                cursor: pointer;
            }

            .btn:hover {
                background-color: #ddd;
            }

            .btn.active {
                background-color: #666;
                color: white;
            }
        </style>
    </head>

    <body>
        <section class="container-fluid" style="margin-top:80px">
            <div class="ms-5">
                <a href=""class="btn btn-dark"> Back </a>
            </div>
            <h1 class="text-center"  style="font-family: 'Shantell Sans', cursive;font-size: 50px">Cart</h1>
        </section>

        <!--MENU-->
        <section id="" class="mt-5">
            <!-- Search input -->
            <input type="text" id="myFilter" class="form-control mb-3 w-25 fs-3 searchbox-input" onkeyup="myFunction()" placeholder="Search for names..">

            <div id="myBtnContainer">
                <button class="btn btn-success active" onclick="filterSelection('all')"> Show all</button>
                <button class="btn" onclick="filterSelection('Seafood')"><h5>Seafood</h5></button>
                <button class="btn" onclick="filterSelection('Drink')"><h5>Drinks</h5></button>
                <button class="btn" onclick="filterSelection('Meat')"><h5>Meats</h5></button>
            </div>

            <!--Menu Food--> 
            <form action="/cart" method="POST" id="formID">
                <div class="row">
                    <%

                        HashMap<String, Integer> cartPayment = (HashMap<String, Integer>) session.getAttribute("cartMenu");

                        for (Map.Entry<String, Integer> entry : cartPayment.entrySet()) {

                            String foodID = entry.getKey();
                            int food_amount = entry.getValue();

                            FoodDAO foodDAO = new FoodDAO();
                            Food f = foodDAO.getFoodByID(foodID);
                            String category_name = foodDAO.getNameByCatID(f.getCategory_id());
//                            float totalEachFoodPrice = food_amount * f.getFood_price();


                    %>
                    <div class="col-3 column <%=category_name%>">
                        <div class="food-item border my-3 text-center content">

                            <!--Food image-->
                            <img src="<%=f.getFood_image()%>" id="image-food" class="card-img-top" height="300" alt="<%= f.getFood_name()%>">

                            <!--Food name, Category name-->
                            <div class="card-body">
                                <h4 class="card-title text-center"><%= f.getFood_name()%></h4>
                                <div class="card-title meatfood"><%=category_name%></div>
                                <p class="card-text text-start" style="white-space: pre-wrap;text-overflow: ellipsis; height: 4em; overflow: hidden;"><%= f.getFood_desc()%></p>

                                <!--Increase - Decrease Button-->
                                <div class="quantity mt-3 d-flex justify-content-center">
                                    <button type="button" class="btn btn-danger minus-btn" data-foodid="<%=f.getFood_id()%>"><i class="fa fa-minus"></i></button>
                                    <input type="text" class="text-center amount_input fs-3" value="<%=food_amount%>" id="<%=f.getFood_id()%>" maxlength="2" size="8">
                                    <button type="button" class="btn btn-success plus-btn" data-foodid="<%=f.getFood_id()%>"><i class="fa fa-plus"></i></button>
                                </div>

                                <!--Food Price-->
                                <div class="d-flex align-items flex-column">
                                    <h3 class="">$<span class="price_value Source Title"><%=f.getFood_price()%></span></h3>
                                </div>

                                <input type="text" name="txtFoodPrice" value="<%=f.getFood_price()%>" hidden="">
                                <input type="text" name="txtFoodID" value="<%=f.getFood_id()%>" hidden="">

                                <!-- DATA SET: >>Food_ID and Food_amount << -->
                                <input type="text" name="FoodID_Amount" value="" id="FoodID_Amount" hidden="">

                            </div>
                        </div> 
                    </div>
                    <%
                        }
                    %>
                </div>
                <!-- End Gallery -->
                <div class="text-center my-5">

                    <button type="submit" style="font-size: 20px" class="btn btn-success" id="btnGoToPayment" name="btnGoToPayment" >Go to payment</button>
                </div>
            </form>
        </section>
        <!--End menu-->

        <script>
            //------------------------- Click (+) button will increase food ---------------------------------
            var minusBtns = document.querySelectorAll(".minus-btn");
            var plusBtns = document.querySelectorAll(".plus-btn");
            var amount_input = document.querySelectorAll(".amount_input");
            var FoodID_Amount = document.querySelector("#FoodID_Amount");
            var form = document.querySelector("#formID");
            var btnGoToPayment = document.querySelector("#btnGoToPayment");

//                ---------------- ( + ) -----------------
            // Khởi tạo biến resultSet để lưu kết quả trước đó
            var resultSet = "";
            for (let i = 0; i < plusBtns.length; i++) {
                plusBtns[i].addEventListener("click", (e) => {

                    var foodID = plusBtns[i].getAttribute("data-foodid");
                    var inputID = document.querySelector("#" + foodID);
                    var getInputTagByID = inputID.getElementsByTagName("input");

                    if (amount_input[i].value < 10) {
                        amount_input[i].value = parseInt(amount_input[i].value) + 1;
                        btnGoToPayment.removeAttribute("hidden");
                        btnGoToPayment.style.display = "inline-block";
                        var newResult = foodID + '/' + amount_input[i].value;
                        resultSet = resultSet + "/" + newResult;
                        FoodID_Amount.setAttribute("value", resultSet);
                    }

                });
            }

//                ---------------- ( - ) -----------------
            for (let i = 0; i < minusBtns.length; i++) {
                minusBtns[i].addEventListener("click", (e) => {
                    var foodID = minusBtns[i].getAttribute("data-foodid");
                    var inputID = document.querySelector("#" + foodID);
                    var getInputTagByID = inputID.getElementsByTagName("input");

                    if (amount_input[i].value > 0) {
                        amount_input[i].value = parseInt(amount_input[i].value) - 1;
                        if (amount_input[i].value == 0) {
                            btnGoToPayment.style.display = "none";

                        }

                        var newResult = foodID + '/' + amount_input[i].value;
                        resultSet = resultSet + "/" + newResult;
                        FoodID_Amount.setAttribute("value", resultSet);
                    }


                });
            }


            //-------------------------- Fillter by Search everything ------------------------------------ -

            $(document).ready(function () {
                $('.searchbox-input').on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $(".food-item").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });

            //-------------------------- Fillter in MENU food ------------------------------------------------
            filterSelection("all")
            function filterSelection(c) {
                var x, i;
                x = document.getElementsByClassName("column");
                if (c == "all")
                    c = "";
                for (i = 0; i < x.length; i++) {
                    w3RemoveClass(x[i], "show");
                    if (x[i].className.indexOf(c) > -1)
                        w3AddClass(x[i], "show");
                }
            }

            function w3AddClass(element, name) {
                var i, arr1, arr2;
                arr1 = element.className.split(" ");
                arr2 = name.split(" ");
                for (i = 0; i < arr2.length; i++) {
                    if (arr1.indexOf(arr2[i]) == -1) {
                        element.className += " " + arr2[i];
                    }
                }
            }

            function w3RemoveClass(element, name) {
                var i, arr1, arr2;
                arr1 = element.className.split(" ");
                arr2 = name.split(" ");
                for (i = 0; i < arr2.length; i++) {
                    while (arr1.indexOf(arr2[i]) > -1) {
                        arr1.splice(arr1.indexOf(arr2[i]), 1);
                    }
                }
                element.className = arr1.join(" ");
            }


            // Add active class to the current button (highlight it)
            var btnContainer = document.getElementById("myBtnContainer");
            var btns = btnContainer.getElementsByClassName("btn");
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click", function () {
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    this.className += " active";
                });
            }

        </script>
    </body>
    <%@include file="header.jsp" %>     
    <%@include file="footer.jsp" %>
</html>
