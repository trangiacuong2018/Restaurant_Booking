<%-- Document : Menu Management Created on : Feb 14, 2023, 7:05:53 PM Author :
Hung --%>
<%@page import="com.DAOS.AccountDAO"%>
<%@page import="com.DAOS.MenuManageDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title>Menu Management</title>
        <meta content="" name="description" />
        <meta content="" name="keywords" />
        <%@include file="import-css-links.jsp"%>
        <style>
            .modal-header {
                background: crimson;
                color: #fff;
            }

            .required:after {
                content: "*";
                color: red;
            }
        </style>
    </head>

    <body>
        <!-- ======= Header ======= -->
        <%@include file= "header-staff.jsp" %>
        <!-- End Header -->

        <%
            //Check if account access this page is staff
            if (account.equals("")) {
                response.sendRedirect(request.getContextPath() + "/");
            } else {
                AccountDAO acc = new AccountDAO();
                if (acc.getAccountById(account).getUser_role().equals("staff")) {
        %>
        <section style="margin-top: 100px">
            <div class="container">
                <div class="text-end">
                    <a href="/orders-management" class="btn btn-danger">Orders Management</a>
                </div>

                <h1 class="text-center">Menu Management</h1>
                <%
                    String msg = "";
                    if (!String.valueOf(request.getAttribute("msg")).equals("")) {
                        msg = String.valueOf(request.getAttribute("msg"));
                    }
                %>
                <h2 class="text-center text-danger"><%= msg%></h2>

                <div class="row">
                    <div class="col text-start">
                        <h2>Category</h2>
                    </div>
                    <!-- Add Food -->
                    <div class="col"></div>
                    <div class="col text-end">
                        <button
                            type="button"
                            class="btn btn-danger"
                            data-bs-toggle="modal"
                            data-bs-target="#AddModal"
                            >
                            Add Food
                        </button>
                        <div class="modal" id="AddModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Add Food</h5>
                                        <button
                                            type="button"
                                            class="btn-close"
                                            data-bs-dismiss="modal"
                                            ></button>
                                    </div>
                                    <div class="modal-body text-start">
                                        <form id="AddForm" method="post" action="menu-management">
                                            <div class="mb-4 row">
                                                <div class="col">
                                                    <label class="form-label"
                                                           >Category of food</label>
                                                    <select name="txtFCat">
                                                        <%
                                                            MenuManageDAO dao = new MenuManageDAO();
                                                            ResultSet rs = dao.getAllCategory();

                                                            while (rs.next()) {
                                                        %>
                                                        <option value="<%=rs.getString("category_id")%>"><%=rs.getString("category_name")%></option>
                                                        <%
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                                <%
                                                    rs = dao.getAllFood();
                                                    int i = 0;

                                                    while (rs.next()) {
                                                        i++;
                                                    }
                                                %>
                                                <input hidden="" type="number" class="form-control"
                                                       name="nextFID" value="<%=i + 1%>" />
                                                <div class="text-end col">
                                                    <label class="form-label">Status</label>
                                                    <select name="txtFStat">
                                                        <option value="Available">Available</option>
                                                        <option value="Expired" disabled="">Expired</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label required">Name of food</label>
                                                <input type="text" name="txtFName" class="form-control" required />
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label required">Food Image</label>
                                                <input type="url" name="txtFImg" class="form-control" required />
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label required">Price</label>
                                                <input type="number" name="txtFPrice" min="1" max="2000" class="form-control" required />
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label required"
                                                       >Food description</label>
                                                <textarea name="txtFDesc" class="form-control" required></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button name="btnFAdd" type="submit" class="btn btn-danger">Add</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Add Food -->
                </div>
                <div class="row">
                    <div class="col">
                        <select class="category">
                            <option value="">All</option>
                            <%
                                ResultSet c = dao.getAllCategory();

                                while (c.next()) {
                            %>
                            <option value="<%=c.getString("category_name")%>">
                                <%=c.getString("category_name")%></option>
                                <%
                                    }
                                %>
                        </select>
                    </div>
                </div>
                <!-- Table Food -->
                <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name of food</th>
                            <th>Food price</th>
                            <th class=" col-4 text-center">Food description</th>
                            <th hidden=""></th>
                            <th hidden=""></th>
                            <th hidden=""></th>
                            <th>Category</th>
                            <th>Status</th>
                            <th class="text-center">Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            rs = dao.getAllFood();
                            i = 0;

                            while (rs.next()) {
                        %>
                        <tr>
                            <td><%=i = i + 1%></td>
                            <td class="fname"><%= rs.getString("food_name")%></td>
                            <td class="fprice"><%= rs.getString("food_price")%></td>
                            <td class="fdesc"><%= rs.getString("food_desc")%></td>
                            <td class="fid" hidden="" ><%= rs.getString("food_id")%></td>
                            <td class="fimg" hidden=""><%= rs.getString("food_image")%></td>
                            <td class="fcat" hidden=""><%= rs.getString("category_id")%></td>
                            <%
                                ResultSet cat = dao.getAllCategory();
                                while (cat.next()) {
                                    if (cat.getString("category_id").equals(rs.getString("category_id"))) {
                            %>
                            <td><%= cat.getString("category_name")%></td>
                            <%
                                    }
                                }
                            %>

                            <td class="fstat"><%= rs.getString("food_status")%></td>
                            <td>
                                <div class="row">
                                    <!-- Edit Food -->
                                    <div class="col">
                                        <button
                                            class="btn btn-success edit"
                                            href=""
                                            data-bs-toggle="modal"
                                            data-bs-target="#UpdateModal"
                                            >
                                            Edit
                                        </button>
                                        <div class="modal" id="UpdateModal">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Update Food</h5>
                                                        <button
                                                            type="button"
                                                            class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            ></button>
                                                    </div>
                                                    <div class="modal-body text-start">
                                                        <form id="UpdateForm" method="post" action="menu-management">
                                                            <div class="mb-4 row">
                                                                <div class="col">
                                                                    <label class="form-label required"
                                                                           >Category of food</label
                                                                    >
                                                                    <select name="txtFCat" class="fcat">
                                                                        <%
                                                                            ResultSet cate = dao.getAllCategory();
                                                                            while (cate.next()) {
                                                                        %>
                                                                        <option value="<%=cate.getString("category_id")%>">
                                                                            <%=cate.getString("category_name")%></option>
                                                                            <%
                                                                                }
                                                                            %>
                                                                    </select>
                                                                </div>
                                                                <input hidden="" type="text" class="form-control fid"
                                                                       name="txtFID" />
                                                                <div class="text-end col">
                                                                    <label class="form-label required">Status</label>
                                                                    <select name="txtFStat" class="fstat">
                                                                        <option value="Available">Available</option>
                                                                        <option value="Expired">Expired</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="form-label required"
                                                                       >Name of food</label>
                                                                <input type="text" name="txtFName" class="form-control fname" required/>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="form-label required">Food Image</label>
                                                                <input type="text" name="txtFImg" class="form-control fimg" required/>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="form-label required">Price</label>
                                                                <input type="number" name="txtFPrice" min="1" max="2000" class="form-control fprice" required/>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="form-label required"
                                                                       >Food description</label>
                                                                <textarea name="txtFDesc" class="form-control fdesc" required></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button name="btnFUpdate" type="submit" class="btn btn-danger">
                                                                    Update
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Edit Food -->

                                    <!-- Confirm Delete -->
                                    <div class="col text-end">
                                        <button
                                            class="btn btn-danger del"
                                            href="#"
                                            data-bs-toggle="modal"
                                            data-bs-target="#Delete"
                                            >
                                            Delete
                                        </button>

                                        <div class="modal" id="Delete">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Confirm delete</h5>
                                                        <button
                                                            type="button"
                                                            class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            ></button>
                                                    </div>
                                                    <div class="modal-body text-start">
                                                        <form id="DeleteForm" method="post" action="menu-management">
                                                            <div class="text-center">
                                                                <h4>Do you want to delete this?</h4>
                                                            </div>
                                                            <input hidden="" type="text" class="form-control fid"
                                                                   name="txtFID" />
                                                            <div class="modal-footer">
                                                                <button name="btnFDel" type="submit" class="btn btn-primary">
                                                                    Yes
                                                                </button>
                                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                                                    No
                                                                </button>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Confirm Delete -->
                                </div>
                            </td>
                        </tr>
                        <%
                                    }
                                } else if (acc.getAccountById(account).getUser_role().equals("admin")) {
                                    response.sendRedirect(request.getContextPath() + "/revenue-management");
                                } else if (acc.getAccountById(account).getUser_role().equals("user")) {
                                    response.sendRedirect(request.getContextPath() + "/");
                                }
                            }
                        %>
                    </tbody>
                </table>
                <!-- End table Food -->
            </div>
        </section>

        <!-- Template Main JS File -->
        <%@include file="import-js-links.jsp" %>

        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript">
            var isValid = true;
            $(document).ready(function () {
                table = $("#example").DataTable();
                $('.category').on('change', function (e) {
                    var cate = $(this).val();
                    $('.category').val(cate);
                    table.column(7).search(cate).draw();
                });
            });
            $('button.edit').on('click', function () {
                var UpdateModal = $('#UpdateModal');

                // now get the values from the table food
                var fcat = $(this).closest('tr').find('td.fcat').html();
                var fname = $(this).closest('tr').find('td.fname').html();
                var fprice = $(this).closest('tr').find('td.fprice').html();
                var fdesc = $(this).closest('tr').find('td.fdesc').html();
                var fimg = $(this).closest('tr').find('td.fimg').html();
                var fstat = $(this).closest('tr').find('td.fstat').html();
                var fid = $(this).closest('tr').find('td.fid').html();

                // and set them in the modal:
                $('.fcat', UpdateModal).val(fcat);
                $('.fname', UpdateModal).val(fname);
                $('.fprice', UpdateModal).val(fprice);
                $('.fdesc', UpdateModal).val(fdesc);
                $('.fimg', UpdateModal).val(fimg);
                $('.fstat', UpdateModal).val(fstat);
                $('.fid', UpdateModal).val(fid);

                // and finally show the modal
                UpdateModal.modal({show: true});

                return true;
            });

            $('button.del').on('click', function () {
                var Delete = $('#Delete');

                // now get the values from the table
                var fid = $(this).closest('tr').find('td.fid').html();

                // and set them in the modal:

                $('.fid', Delete).val(fid);

                // and finally show the modal
                Delete.modal({show: true});

                return true;
            });
        </script>
        <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap5.min.js"></script>
    </body>
</html>
