<%-- 
    Document   : Order Management
    Created on : Feb 14, 2023, 10:06:19 PM
    Author     : Hung
--%>

<%@page import="com.DAOS.AccountDAO"%>
<%@page import="javax.swing.text.Document"%>
<%@page import="com.models.Order_items"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.DAOS.OrderManageDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="com.models._Table"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Order Management</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <%@include file="import-css-links.jsp"%>
        <style>
            .modal-header {
                background: crimson;
                color: #fff;
            }

            .required:after {
                content: "*";
                color: red;
            }
        </style>
    </head>


    <body>
        <!-- ======= Header ======= -->
        <%@include file= "header-staff.jsp" %>
        <!-- End Header -->

        <%  cookies = request.getCookies();
            String accountId = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("account")) {
                        accountId = cookie.getValue();
                        break;
                    }
                }
            }
            if (account.equals("")) {
                response.sendRedirect(request.getContextPath() + "/");
            } else {
                AccountDAO acc = new AccountDAO();
                if (acc.getAccountById(accountId).getUser_role().equals("staff")) {
        %>
        <section style="margin-top: 100px">
            <div class="container-fluid">
                <div class="text-end"><a href="/menu-management" class="btn btn-danger">Menu Management</a></div>

                <!-- List of tables -->
                <div class="form-group mx-5 my-5 p-lg-4 rounded-4" style="background-color: #6699FF;">
                    <h1 class="container" style="text-align: left; font-family: 'Shantell Sans', cursive;">View table status</h1>
                    <h3 class="text-center">Front-door</h3>

                    <div class="row text-center">

                        <%
                            List<_Table> list = (List<_Table>) request.getAttribute("list");
                            for (_Table t : list) {
                        %>
                        <div class="col-2">
                            <form id="ChangeTableStatus" method="post" action="orders-management">
                                <button name="btnTableID" class="btn btn-dark my-2 mx-1 rounded-4
                                        <%= t.getTable_status().equalsIgnoreCase("busy") ? "btn-danger" : ""%>
                                        "style="width: 20rem;height: 10rem" value="<%= t.getTable_id()%>">
                                    <h4>Table <%= t.getTable_number()%></h4>
                                </button>
                            </form>
                        </div>
                        <%
                            }
                        %>
                    </div>

                    <h3 class="text-center">Back-door</h3>
                    <div class="mb-3" style="font-family: 'Shantell Sans', cursive;">
                        <h3>Table Status: </h3>

                        <div class="d-flex container-fluid">
                            <button type="button" class="btn btn-danger my-2 mx-2" style="width: 20rem;"><h5>Busy</h5></button>
                            <button type="button" class="btn btn-dark my-2 mx-2" style="width: 20rem;"><h5>Available</h5></button>
                        </div>
                    </div>
                </div>
                <!-- End list of tables -->

                <h1 class="container" style="text-align: left; font-family: 'Shantell Sans', cursive;"">Orders Management</h1>
                <!-- List of orders (Status: Processing) -->
                <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Phone number</th>
                            <th class="text-center">Date Check-in</th>
                            <th class="text-center">Time Check-in</th>
                            <th class="text-center">People</th>
                            <th class="text-center">Table</th>
                            <th class="text-center">Payment Time</th>
                            <th class="text-center">Food</th>
                            <th class="text-center">Note</th>
                            <th class="text-center">Total</th>
                            <th hidden=""></th>
                            <th hidden=""></th>
                            <th class="text-center col-2">Confirm</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            OrderManageDAO dao = new OrderManageDAO();
                            ResultSet rs = dao.getAllOrder();
                            int i = 0;
                            String temp = "";
                            while (rs.next()) {
                        %>

                        <tr>
                            <td><%= ++i%></td>
                            <td><%= rs.getString("user_FullName")%></td>
                            <td><%= rs.getString("user_phone")%></td>
                            <td><%= rs.getString("order_tableDate")%></td>
                            <td><%= rs.getString("order_tableTimeCheckin")%></td>
                            <td><%= rs.getString("order_tablePeople")%></td>
                            <td><%= rs.getString("order_tableNumber")%></td>
                            <td><%= rs.getString("invoice_timePayment")%></td>
                            <td>
                                <form id="OrderDetail" method="post" action="orders-management">
                                    <a href="#gg"> <button name="btnODetail" id="ShowDetail" value="<%= rs.getString("order_id")%>" class="btn btn-danger">Detail</button></a>
                                </form>
                            </td>
                            <td><%= rs.getString("order_tableNote")%></td>
                            <td><%= rs.getString("invoice_total")%></td>
                            <td class="oid" hidden=""><%= rs.getString("order_id")%></td>
                            <td class="tid" hidden="" ><%= rs.getString("table_id")%></td>
                            <td>
                                <div class="row">
                                    <!-- Confirm button -->
                                    <div class="col">
                                        <form id="ChangeOrderStatus" method="post" action="orders-management">
                                            <input type="text" class="form-control oid"
                                                   name="txtOID" hidden="" value="<%= rs.getString("order_id")%>"/>
                                            <button name="btnOCon" type="submit" class="btn btn-success">Confirm</button>
                                        </form>
                                    </div>
                                    <!-- End Confirm button -->
                                    
                                    <!-- Delete button -->
                                    <div class="text-end col">
                                        <button class="btn btn-danger del" data-bs-toggle="modal" data-bs-target="#Delete">Delete</button>
                                        <div class="modal" id="Delete">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Confirm delete</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                                    </div>
                                                    <div class="modal-body text-start">
                                                        <form id="DeleteForm" method="post" action="orders-management">
                                                            <div class="text-center">
                                                                <h4>Do you want to delete this?</h4>
                                                            </div>
                                                            <input type="text" hidden="" class="form-control oid"
                                                                   name="txtOID" />
                                                            <input type="text" hidden="" class="form-control tid"
                                                                   name="txtTID" />
                                                            <div class="modal-footer">
                                                                <button name="btnODel" type="submit" class="btn btn-primary">
                                                                    Yes
                                                                </button>
                                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
                                                                    No
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Delete button -->
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <!-- End list of orders -->

                <!-- List of food of 1 order -->
                <%
                    temp = String.valueOf(request.getAttribute("oid"));
                    if (!temp.equals("null")) {
                %>
                <div id="DetailTable gg">
                    <h2 style="text-align: left; font-family: 'Shantell Sans',
                        cursive;"">Order Detail of Table <%= dao.getTableNumberByOID(temp)%></h2>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Food Name</th>
                                <th class="text-center">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                ResultSet orderitems = dao.getItemsByOID(temp);
                                while (orderitems.next()) {
                            %>
                            <tr>
                                <td><%= orderitems.getString("food_name")%></td>
                                <td><%= orderitems.getString("food_amount")%></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                </div>
                <%
                    }
                %>
                <!-- End list of food of 1 order -->
            </div>
        </section>
        <%
                } else if (acc.getAccountById(accountId).getUser_role().equals("admin")) {
                    response.sendRedirect(request.getContextPath() + "/revenue-management");
                } else if (acc.getAccountById(accountId).getUser_role().equals("user")) {
                    response.sendRedirect(request.getContextPath() + "/");
                }
            }
        %>

        <!-- Vendor JS Files -->
        <%@include file="import-js-links.jsp" %>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#example").DataTable();
                $("#ShowDetail").click(function () {
                    $("#DetailTable").css("display", "block");
                });
            });

            $('button.del').on('click', function () {
                var Delete = $('#Delete');
                // now get the values from the table
                var oid = $(this).closest('tr').find('td.oid').html();
                var tid = $(this).closest('tr').find('td.tid').html();

                // and set them in the modal:
                $('.oid', Delete).val(oid);
                $('.tid', Delete).val(tid);

                // and finally show the modal
                Delete.modal({show: true});
                return true;
            });
        </script>
        <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap5.min.js"></script>
    </body>
</html>
