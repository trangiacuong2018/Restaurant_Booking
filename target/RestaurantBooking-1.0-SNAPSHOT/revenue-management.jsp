<%@page import="com.DAOS.AccountDAO"%>
<%@page import="com.models._User"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.DAOS.RevenueManagementDAO"%>
<%@page import="java.util.concurrent.atomic.AtomicInteger"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Revenue Management</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">


        <%@include file="import-js-links.jsp" %>
        <%@include file="import-css-links.jsp" %>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                let table = new DataTable('#example');
            });
        </script>
    </head>

    <body>



        <div class="container-xxl bg-white p-0">
            <%@include file= "header-admin.jsp" %>
            <%
                cookies = request.getCookies();
                String accountId = "";
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("account")) {
                            accountId = cookie.getValue();
                            break;
                        }
                    }
                }
                if (account.equals("")) {
                    response.sendRedirect(request.getContextPath() + "/");
                } else {
                    AccountDAO acc = new AccountDAO();
                    if (acc.getAccountById(accountId).getUser_role().equals("admin")) {
            %>

            <!-- Header End -->
            <div style="margin-top: 100px" class="container  ">
                <div  class="container my-5 pt-5 pb-4 d-flex flex-row-reverse ">
                    <div class="btn-group btn-group-lg mx-5">
                        <a href="<%= request.getContextPath()%>/staff-management"> <button type="button" class="btn btn-danger">Staff account management</button></a>
                    </div>
                    <div class="btn-group btn-group-lg ">
                        <a href="/user-management"><button type="button" class="btn btn-danger">User account management</button></a>

                    </div>
                </div>
            </div>
            <!-- Header End -->
            <h1 class="text-center mb-5">Revenue Management</h1>
            <div>
                <form action="/revenue-management" method="POST">
                    <div class="row ">
                        From
                        <div class="col-md-3">
                            <input type="date" class=" text-center form-control" name="from">
                        </div>
                        to
                        <div class="col-md-3">
                            <input type="date"  class="text-center form-control"  name="to">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" name="btn_filter" value="Fileter" class="btn btn-danger">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- About Start -->
            <div class="container-xxl py-5">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Date time</th>
                            <th scope="col">People</th>
                            <th scope="col">Table</th>
                            <th scope="col">Total order</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            if (request.getParameter("btn_filter") != null) {
                                if (request.getParameter("from") != "" && request.getParameter("to") != "") {

                                    AtomicInteger count = new AtomicInteger(1);
                                    String min = request.getParameter("from");
                                    String max = request.getParameter("to");
                                    RevenueManagementDAO dAO = new RevenueManagementDAO();
                                    ResultSet rs = dAO.getAllRevenueByDate(min, max);
                                    float total = 0;
                                    while (rs.next()) {
                                        _User user = dAO.getUserByID(rs.getString("user_id"));
                                        total += rs.getFloat("invoice_total");
                        %>
                        <tr>
                            <th scope="row"><%= count.getAndIncrement()%></th>
                            <td><%= user.getUser_fullName()%></td>
                            <td><%= user.getUser_phone()%></td>
                            <td><%=  rs.getString("invoice_timePayment")%></td>
                            <td><%= rs.getString("order_tablePeople")%></td>
                            <td><%= rs.getString("order_tableNumber")%></td>
                            <td><%= rs.getString("invoice_total")%></td>
                        </tr>
                        <%
                            }
                        %>
                        <tr>
                            <th colspan="6" class="text-center">Total Revenue</th>
                            <td><%= total%></td>
                        </tr>
                        <%                            } else {
                        %>
                    <div class="alert alert-danger" role="alert">
                        Filled String from and to
                    </div>
                    <%
                        }
                    } else {
                        AtomicInteger count = new AtomicInteger(1);
                        RevenueManagementDAO dAO = new RevenueManagementDAO();
                        ResultSet rs = dAO.getAllRevenue();
                        float total = 0;
                        while (rs.next()) {
                            _User user = dAO.getUserByID(rs.getString("user_id"));
                            total += rs.getFloat("invoice_total");
                    %>
                    <tr>
                        <th scope="row"><%= count.getAndIncrement()%></th>
                        <td><%= user.getUser_fullName()%></td>
                        <td><%= user.getUser_phone()%></td>
                        <td><%=  rs.getString("invoice_timePayment")%></td>
                        <td><%= rs.getString("order_tablePeople")%></td>
                        <td><%= rs.getString("order_tableNumber")%></td>
                        <td><%= rs.getString("invoice_total")%></td>
                    </tr>

                    <%
                        }
                    %>
                    <tr>
                        <th colspan="6" class="text-center">Total Revenue</th>
                        <td><%= total%></td>
                    </tr>
                    <%
                        }
                    %>
                    </tbody>
                </table>
            </div>
            <!-- About End -->
            <%
                    } else if (acc.getAccountById(accountId).getUser_role().equals("staff")) {
                        response.sendRedirect(request.getContextPath() + "/orders-management");
                    } else if (acc.getAccountById(accountId).getUser_role().equals("user")) {
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }
            %>




        </div>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/wow/wow.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>

</html>