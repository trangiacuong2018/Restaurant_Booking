<%@page import="com.DAOS.AccountDAO"%>
<%@page import="java.util.concurrent.atomic.AtomicInteger"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.DAOS.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Staff Management</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <%@include file="import-css-links.jsp"%>
        <style>
            .modal-header {
                background: crimson;
                color: #fff;
            }

            .required:after {
                content: "*";
                color: red;
            }
        </style>
        <script type="text/javascript">
            var patt_email = /^[a-zA-Z]+\w*([.-_]\w+)*\@[a-zA-Z]+\w*([.-_]\w+)*(\.\w+)+$/;
            var patt_phone = /^0[1-9]\d{8,10}$/;
            function checkAllData() {
                var phone = document.getElementById("phone").value;
                var errorArray = [];
                if (patt_phone.test(phone) === false)
                    errorArray.push("Phone number is invalid");
                var errorMessage = document.getElementById("txtError");
                if (errorArray.length != 0) {
                    var strMsg = "Please check invalid data:";
                    strMsg += "</ol>";
                    for (const index in errorArray) {
                        strMsg += "<li>" + errorArray[index] + "</li>";
                    }
                    strMsg += "</ol>";
                    errorMessage.innerHTML = strMsg;
                    errorMessage.style.color = "#f00";
                    return false;
                }
            }
            var patt_phone1 = /^0[1-9]\d{8,10}$/;
            function checkAllData1() {
                var phone = document.getElementById("phone1").value;
                var errorArray1 = [];
                if (patt_phone1.test(phone) === false)
                    errorArray1.push("Phone number is invalid");
                console.log(phone);
                var errorMessage = document.getElementById("txtError1");
                if (errorArray1.length != 0) {
                    var strMsg = "Please check invalid data:";
                    strMsg += "</ol>";
                    for (const index in errorArray1) {
                        strMsg += "<li>" + errorArray1[index] + "</li>";
                    }
                    strMsg += "</ol>";
                    errorMessage.innerHTML = strMsg;
                    errorMessage.style.color = "#f00";
                    return false;
                }
            }


        </script>
    </head>


    <body>
        <!-- ======= Header ======= -->
        <%@include file= "header-admin.jsp" %>

        <%
            cookies = request.getCookies();
            String accountId = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("account")) {
                        accountId = cookie.getValue();
                        break;
                    }
                }
            }
            if (account.equals("")) {
                response.sendRedirect(request.getContextPath() + "/");
            } else {
                AccountDAO acc = new AccountDAO();
                if (acc.getAccountById(accountId).getUser_role().equals("admin")) {
        %>
        <!-- End Header -->
        <!-- Header End -->
        <div style="margin-top: 50px" class="container  ">
            <div class="container my-5 pt-5 pb-4 d-flex flex-row-reverse ">
                <div class="btn-group btn-group-lg ">
                    <a href="/user-management"><button type="button" class="btn btn-danger">User account management</button></a>
                </div>
                <div class="btn-group btn-group-lg ">
                    <button type="button" class="btn "></button>
                </div>
                <div class="btn-group btn-group-lg ">
                    <a href="/revenue-management"><button type="button" class="btn btn-danger">Revenue management</button></a>
                </div> <div class="btn-group btn-group-lg ">
                    <button type="button" class="btn "></button>
                </div>
            </div>
        </div>

        <section class="container ">
            <h1 class="text-center">Staff Account Management</h1>
            <%                String msg = "";
                if (!String.valueOf(request.getAttribute("msg")).equals("")) {
                    msg = String.valueOf(request.getAttribute("msg"));
                }
            %>
            <h2 class="text-center text-danger"><%= msg%></h2>

            <!--Update Staff Modal-->
            <div class="d-flex justify-content-end mb-3 col">
                <button
                    class="btn btn-danger"
                    href="#"
                    data-bs-toggle="modal"
                    data-bs-target="#AddModal"
                    >
                    ADD STAFF
                </button>
                <div class="modal" id="AddModal" tabindex="-1">
                    <div class="modal-dialog">
                        <div  class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">ADD STAFF</h3>  
                                <button
                                    type="button"
                                    class="btn-close"
                                    data-bs-dismiss="modal"
                                    ></button>
                            </div>
                            <p class="error" id="txtError"></p> 
                            <form action="staff-management" method="post"  onsubmit="return checkAllData()">
                                <div class="modal-body text-start">

                                    <div class="mb-3">
                                        <label class="form-label required"
                                               >FullName</label
                                        >
                                        <input type="text" name="full_name" maxlength="100" required id="full_name" class="form-control" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label required">Email</label>
                                        <input type="email" name="email" required id="email" class="form-control" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label required">Phone</label>
                                        <input type="phone" name="phone" required id="phone" class="form-control" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label required">Username</label>
                                        <input type="username" name="username" minlength="3" maxlength="100" required id="username" class="form-control" />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label required">Password</label>
                                        <input type="password" required minlength="6" maxlength="50" name="password" id="password" class="form-control" />
                                    </div>


                                </div>
                                <div class="modal-footer text-center">
                                    <button type="submit" name="btn_add_staff" class="btn btn-danger">
                                        ADD
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End UpdateStaff Modal-->
            <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Full name</th>
                        <th>Email</th>
                        <th>Phone number</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th class="text-center">Update</th>
                    </tr>
                </thead>
                <tbody>


                    <%
                        UserDAO userDAO = new UserDAO();
                        ResultSet rs = userDAO.getAllUser();
                        AtomicInteger count = new AtomicInteger(1);
                        while (rs.next()) {
                            if (rs.getString("user_role").equals("staff")) {
                    %>
                    <tr>
                        <td><%= count.getAndIncrement()%></td>
                        <td><%= rs.getString("user_fullName")%></td>
                        <td><%= rs.getString("email")%></td>
                        <td><%= rs.getString("user_phone")%></td>
                        <td><%= rs.getString("username")%></td>
                        <td><%= rs.getString("password")%></td>
                        <td>
                            <div class="row mx-2">
                                <!--Update Staff Modal-->
                                <div class="col">
                                    <button
                                        class="btn btn-success"
                                        href="#"
                                        data-bs-toggle="modal"
                                        data-bs-target="#UpdateModal-<%= rs.getString("user_id")%>"
                                        >
                                        Update
                                    </button>
                                    <div class="modal" id="UpdateModal-<%= rs.getString("user_id")%>">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title">Update Staff information</h3>
                                                    <button
                                                        type="button"
                                                        class="btn-close"
                                                        data-bs-dismiss="modal"
                                                        ></button>
                                                </div>
                                                <p class="error" id="txtError1"></p> 
                                                <form action="staff-management" method="post" onsubmit="return checkAllData1()">
                                                    <div class="modal-body text-start">
                                                        <input type="hidden" name="user_id" readonly value="<%= rs.getString("user_id")%>" class="form-control" />
                                                        <div class="mb-3">
                                                            <label class="form-label required"
                                                                   >FullName</label
                                                            >
                                                            <input type="text" name="full_name" maxlength="100" required id="full_name" value="<%= rs.getString("user_fullName")%>" class="form-control" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label required">Email</label>
                                                            <input type="email" name="email" required id="email" value="<%= rs.getString("email")%>" class="form-control" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label required">Phone</label>
                                                            <input type="phone" name="phone" required id="phone1" value="<%= rs.getString("user_phone")%>" class="form-control" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label required">Username</label>
                                                            <input type="username" name="username" minlength="3" maxlength="100"  value="<%= rs.getString("username")%>" readonly class="form-control" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label required">Password</label>
                                                            <input type="password" name="password" minlength="6" maxlength="50"  required id="password" value="<%= rs.getString("password")%>" class="form-control" />
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer text-center">
                                                        <button type="submit" name="btn_update_staff" class="btn btn-danger">
                                                            Update
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End UpdateStaff Modal-->

                                <div class="col text-end">
                                    <button
                                        class="btn btn-danger"
                                        href="#"
                                        data-bs-toggle="modal"
                                        data-bs-target="#Delete<%= rs.getString("user_id")%>"
                                        >
                                        Delete
                                    </button>
                                    <div class="modal" id="Delete<%= rs.getString("user_id")%>">
                                        <form action="staff-management" method="post">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Confirm delete</h5>
                                                        <button
                                                            type="button"
                                                            class="btn-close"
                                                            data-bs-dismiss="modal"
                                                            ></button>
                                                    </div>
                                                    <div class="modal-body text-start">
                                                        <div class="text-center">
                                                            <h4>Do you want to delete this?</h4>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="user_id" value="<%= rs.getString("user_id")%>">
                                                        <button type="submit" name="btn_delete_staff" class="btn btn-primary">
                                                            Yes
                                                        </button>

                                                        <a href="<%= request.getContextPath()%>/staff-managment"> <button type="button" class="btn btn-danger">
                                                                No
                                                            </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <%
                                    }
                                }
                            } else if (acc.getAccountById(accountId).getUser_role().equals("staff")) {
                                response.sendRedirect(request.getContextPath() + "/orders-management");
                            } else if (acc.getAccountById(accountId).getUser_role().equals("user")) {
                                response.sendRedirect(request.getContextPath() + "/");
                            }
                        }
                    %>

                </tbody>
            </table>
        </section>
        <a href="#" class="scroll-top d-flex align-items-center
           justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <div id="preloader"></div>

        <!-- Vendor JS Files -->
        <%@include file="import-js-links.jsp" %>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#example').DataTable();
                                                    });
        </script>
        <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap5.min.js"></script>
    </body>
</html>



