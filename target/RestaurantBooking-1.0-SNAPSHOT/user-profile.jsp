<%-- 
    Document   : User Profile
    Created on : Feb 21, 2023, 1:43:12 PM
    Author     : Hung
--%>

<%@page import="com.models._User"%>
<%@page import="com.DAOS.AccountDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Profile</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <%@include file="import-css-links.jsp"%>
        <style>
            .disbale {
                display: none;
            }
            
            .active {
                display: block;
            }
            
            .success {
                color: green;
            }

            .error {
                color: red;
            }

            .btn-booking {
                color: white;
            }
            
            .from {
                display: flex;
                justify-content: center;
                align-items: center;
            }
            
            .txt_username {
                background-color: gray;
                width: 100%;
                padding: 10px 0;
                color: black;
                border-radius: 10px;
            }
            
            .txt_password {
                background-color: gray;
                width: 100%;
                padding: 10px 0;
                color: black;
                border-radius: 10px;
            }
            
            .txt_fullname {
                background-color: gray;
                width: 100%;
                padding: 10px 0;
                color: black;
                border-radius: 10px;
            }
            
            .txt_phone {
                background-color: gray;
                width: 100%;
                padding: 10px 0;
                color: black;
                border-radius: 10px;
            }
            
            .txt_email {
                background-color: gray;
                width: 100%;
                padding: 10px 0;
                color: black;
                border-radius: 10px;
            }
        </style>
    </head>

    <body>
        <!-- ======= Header ======= -->
        <%@include file= "header.jsp" %>
        <!-- End Header -->

        <%
            AccountDAO accountDAO = new AccountDAO();
            String id = "";
            for (Cookie i : request.getCookies()) {
                id = i.getValue();
            }
            String role = accountDAO.getAccountById(id).getUser_role();
        %>

        <!-- title -->

        <div class=" text-center justify-content-center" style="margin-top: 150px">
            <div class="row">
                <div class="col"></div>

                <%
                    if (role.equalsIgnoreCase("user")) { %>
                <div class="col"><button class="btn btn-danger"><a class="btn-booking" href="/account/booking-history">Booking History</a></button></div>
                <% }%>

            </div>
        </div>
        <!-- form -->
        <section class="container w-50 border" style="margin-top: 10px">
            <div class=" text-center"><h3>User Profile</h3></div>

            <form action="/account" method="POST" name="Form" onsubmit="return validateForm()">

                <!-- username -->
                <div class="form-group row">
                    <div class="col">
                        <label>Username</label>
                        <input type="text" class="form-control" id="inputUsername" name="inputUsername" readonly value="<%=accountDAO.getAccountById(id).getUsername()%>">
                    </div>
                </div>

<!--                 password 
                <div class="form-group row">
                    <div class="col">
                        <label>Password</label>
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword">
                        <span id="span-pw" class="disbale">Password must be from 6 to 50 characters</span><br/>
                        <input type="checkbox" onclick="tooglePassword()">  Show password
                    </div>
                </div>-->

                <!-- full name -->
                <div class="form-group row">
                    <div class="col">
                        <label>Full name</label>
                        <%
                            if (role.equalsIgnoreCase("user")) {%>
                        <input type="text" class="form-control" id="inputFullname" name="inputFullname" value="<%=accountDAO.getAccountById(id).getUser_fullName()%>">
                        <% }%>
                        <input type="text" class="form-control" id="inputFullname" name="inputFullname" readonly="" value="<%=accountDAO.getAccountById(id).getUser_fullName()%>">
                        <span id="span-fn" class="disbale"><p class="error">Fullname can not be empty and less than 100 characters</p></span>
                    </div>
                </div>

                <!-- email -->
                <div class="form-group row">
                    <div class="col">
                        <label>Email</label>
                        <%
                            if (role.equalsIgnoreCase("user")) {%>
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" value="<%=accountDAO.getAccountById(id).getEmail()%>">
                        <% }%>
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" readonly value="<%=accountDAO.getAccountById(id).getEmail()%>">
                    </div>
                </div>

                <!-- phone -->
                <div class="form-group row">
                    <div class="col">
                        <label>Phone</label>
                        <%
                            if (role.equalsIgnoreCase("user")) {%>
                        <input type="text" class="form-control" id="inputPhone" name="inputPhone" value="<%=accountDAO.getAccountById(id).getUser_phone()%>">
                        <% }%>
                        <input type="text" class="form-control" id="inputPhone" name="inputPhone" readonly value="<%=accountDAO.getAccountById(id).getUser_phone()%>">
                        <span id="span-phone" class="disbale"><p class="error">Your phone must be 10 digits</p></span>
                    </div>
                </div>

                <!-- button register -->
                <div class="register_button text-center my-5">
                    <%
                        if (role.equalsIgnoreCase("user")) { %>
                    <button type="submit" name="btn_update" class="btn btn-danger">Update Profile</button>
                    <% }%>

                </div>
            </form>
        </section>

        <!-- Bootstrap JS Library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
                                function regexPhoneNumber(phone) {
                                    const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
                                    return phone.match(regexPhoneNumber) ? true : false;
                                }
                                function validateForm() {
                                    var inputFullName = document.forms["Form"]["inputFullname"].value;
                                    var inputEmail = document.forms["Form"]["inputEmail"].value;
                                    var inputPhone = document.forms["Form"]["inputPhone"].value;
                                    if (inputFullName === null || inputFullName === "" || inputFullName.length > 100) {
                                        document.getElementById("span-fn").classList.remove("disbale");
                                        return false;
                                    } else {
                                        document.getElementById("span-fn").classList.add("disbale");
                                    }
                                    if (inputEmail === null || inputEmail === "") {
                                        alert("Please enter email");
                                        return false;
                                    }
                                    if (regexPhoneNumber(inputPhone) === false) {
                                        document.getElementById("span-phone").classList.remove("disbale");
                                        return false;
                                    } else {
                                        document.getElementById("span-phone").classList.add("disbale");
                                    }
                                }
        </script>

        <!-- Template Main JS File -->
        <%@include file="import-js-links.jsp" %>


    </body>
</html>
